package io.gitlab.donespeak.tutorial.quartz.sample;

import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

import static org.quartz.SimpleScheduleBuilder.simpleSchedule;

/**
 * @author Yang Guanrong
 */
public class QuickStart {

    public static class HelloJob implements Job {

        @Override
        public void execute(JobExecutionContext context) throws JobExecutionException {

        }
    }

    public static void main(String[] args) {
        // define the job and tie it to our HelloJob class
        JobDetail job = JobBuilder.newJob(HelloJob.class)
            .withIdentity("myJob", "group1") // name "myJob", group "group1"
            .build();

        // Trigger the job to run now, and then every 40 seconds
        Trigger trigger = TriggerBuilder.newTrigger()
            .withIdentity("myTrigger", "group1")
            .startNow()
            .withSchedule(simpleSchedule()
                .withIntervalInSeconds(40)
                .repeatForever())
            .build();

        // Tell quartz to schedule the job using our trigger
        // sched.scheduleJob(job, trigger);
    }
}
