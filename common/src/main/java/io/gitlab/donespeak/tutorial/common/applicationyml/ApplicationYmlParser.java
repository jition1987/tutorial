package io.gitlab.donespeak.tutorial.common.applicationyml;

import org.apache.commons.lang3.StringUtils;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;

/**
 * @author Yang Guanrong
 * @date 2019/11/12 18:02
 */
public class ApplicationYmlParser {

    public static <T> T parse(String profile, Class<T> clazz) {
        profile = StringUtils.isEmpty(profile)? "": "-" + profile;
        String applicationYaml = "application" + profile + ".yml";

        Yaml yaml = new Yaml();
        InputStream inputStream = clazz.getClassLoader().getResourceAsStream(applicationYaml);

        return yaml.load(inputStream);
    }
}
