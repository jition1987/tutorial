package io.gitlab.donespeak.tutorial.bugs.systembugs.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author Yang Guanrong
 * @date 2020/03/27 22:17
 */
@Slf4j
@RestController
@RequestMapping("/executor/")
public class ExecutorTestController {

	@GetMapping("/many-pool")
	public void createExecutors(int num, boolean close) {
		List<ExecutorService> ess = new ArrayList<>();
		for(int n = 0; n < num; n ++) {
			ExecutorService es = Executors.newFixedThreadPool(1);
			es.submit(() -> {

				return;
			});
			ess.add(es);
		}
		for(ExecutorService es: ess) {
			es.shutdown();
			try {
				es.awaitTermination(10, TimeUnit.SECONDS);
			} catch (Exception e) {
				log.error("Fail to invoke awaitTermination()" + e.getMessage(), e);
			}
		}
	}
}
