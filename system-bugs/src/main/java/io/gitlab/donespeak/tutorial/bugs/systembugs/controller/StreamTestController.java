package io.gitlab.donespeak.tutorial.bugs.systembugs.controller;

import org.apache.commons.io.IOUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


/**
 * @author Yang Guanrong
 * @date 2020/03/27 22:34
 */
@RestController
@RequestMapping("/stream")
public class StreamTestController {

	private static final String FILE_CONTENT = "This is a file.";

	@GetMapping("/file")
	public void getFile(boolean closeIn, boolean closeOut, HttpServletResponse response) throws IOException {
		// TODO response.getOutputStream(); 是否需要关闭
		OutputStream out = response.getOutputStream();
		for(int i = 0; i < 10; i ++) {
			StringBuilder tmp = new StringBuilder();
			for(int a = 0; a < 100; a ++) {
				tmp.append(FILE_CONTENT);
			}
			InputStream input = new ByteArrayInputStream(tmp.toString().getBytes());
			IOUtils.copy(input, out);
			if(closeIn) {
				if(input != null) {
					input.close();
				}
			}
		}
		out.flush();
		if(closeOut == true) {
			if(out != null) {
				out.close();
			}
		}
	}
}
