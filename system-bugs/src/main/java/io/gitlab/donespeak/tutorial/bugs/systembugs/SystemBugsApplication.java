package io.gitlab.donespeak.tutorial.bugs.systembugs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SystemBugsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SystemBugsApplication.class, args);
	}

}
