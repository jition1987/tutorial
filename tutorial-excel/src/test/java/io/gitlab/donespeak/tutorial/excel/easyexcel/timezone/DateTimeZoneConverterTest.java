package io.gitlab.donespeak.tutorial.excel.easyexcel.timezone;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

/**
 * @author DoneSpeak
 * @date 2019/11/21 22:01
 */
public class DateTimeZoneConverterTest {
    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    @NoArgsConstructor
    public static class TheDate {
        @DateTimeFormat("yyyy-MM-dd hh:mm:ss:SSS")
        @ExcelProperty(index = 0)
        private Date date;

        @DateTimeFormat("yyyy-MM-dd hh:mm:ss:SSS")
        @DateTimeZone("Asia/Tokyo")
        @ExcelProperty(index = 1)
        private Date jpDate;
    }

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    /**
     * https://www.zeitverschiebung.net/cn/all-time-zones.html
     */
    private static final String TIME_ZONE_ID_US_CENTRAL = "US/Central";
    private static final String TIME_ZONE_ID_ETC_UTC = "Etc/UTC";
    private static final String TIME_ZONE_ID_JP = "Asia/Tokyo"; // UTC+9

    public File getTestDirectory() {
        return temporaryFolder.getRoot();
    }

    @Test
    public void testDateTimeZoneStringConverter() {
        File file = new File(getTestDirectory(), "easyexcel-test-dateTimeZoneStringConverter.xlsx");

        if(file.exists()) {
            file.delete();
        }
        List<TheDate> listOriginal = data();

        // 用 US/Central 去写入Excel中的时间
        EasyExcel.write(file, TheDate.class)
            .registerConverter(new DateTimeZoneStringConverter(TIME_ZONE_ID_US_CENTRAL))
            .sheet("theDate").doWrite(listOriginal);

        // 用 US/Central 去读取Excel中的时间
        List<TheDate> listUsCentralWriteUsCentralRead = EasyExcel.read(file)
            .registerConverter(new DateTimeZoneStringConverter(TIME_ZONE_ID_US_CENTRAL))
            .head(TheDate.class).sheet().doReadSync();

        assertListEquals(listOriginal, listUsCentralWriteUsCentralRead);

        // 用 UTC 时区去读取Excel中的时间
        List<TheDate> listUsCentralWriteEtcUtcRead = EasyExcel.read(file)
            .registerConverter(new DateTimeZoneStringConverter(TIME_ZONE_ID_ETC_UTC))
            .head(TheDate.class).sheet().doReadSync();

        System.out.println(listUsCentralWriteEtcUtcRead);

        assertTimeSpan(collectDate(listOriginal, d -> d.getDate()), collectDate(listUsCentralWriteEtcUtcRead, d -> d.getDate()),
            TIME_ZONE_ID_US_CENTRAL, TIME_ZONE_ID_ETC_UTC);
        assertTimeSpan(collectDate(listOriginal, d -> d.getJpDate()), collectDate(listUsCentralWriteEtcUtcRead, d -> d.getJpDate()),
            TIME_ZONE_ID_JP, TIME_ZONE_ID_JP);
    }

    @Test
    public void testDateTimeZoneNumberConverter() {
        File file = new File(getTestDirectory(), "easyexcel-test-dateTimeZoneNumberConverter.xlsx");

        if(file.exists()) {
            file.delete();
        }
        List<TheDate> listOriginal = data();

        // 用 US/Central 去写入Excel中的时间
        EasyExcel.write(file, TheDate.class)
            .registerConverter(new DateTimeZoneNumberConverter(TIME_ZONE_ID_US_CENTRAL))
            .sheet("theDate").doWrite(listOriginal);

        // 用 US/Central 去读取Excel中的时间
        List<TheDate> listUsCentralWriteUsCentralRead = EasyExcel.read(file)
            .registerConverter(new DateTimeZoneNumberConverter(TIME_ZONE_ID_US_CENTRAL))
            .head(TheDate.class).sheet().doReadSync();

        assertListEquals(listOriginal, listUsCentralWriteUsCentralRead);

        // 用 UTC 时区去读取Excel中的时间
        List<TheDate> listUsCentralWriteEtcUtcRead = EasyExcel.read(file)
            .registerConverter(new DateTimeZoneNumberConverter(TIME_ZONE_ID_ETC_UTC))
            .head(TheDate.class).sheet().doReadSync();

        assertTimeSpan(collectDate(listOriginal, d -> d.getDate()), collectDate(listUsCentralWriteEtcUtcRead, d -> d.getDate()),
            TIME_ZONE_ID_US_CENTRAL, TIME_ZONE_ID_ETC_UTC);
        assertTimeSpan(collectDate(listOriginal, d -> d.getJpDate()), collectDate(listUsCentralWriteEtcUtcRead, d -> d.getJpDate()),
            TIME_ZONE_ID_JP, TIME_ZONE_ID_JP);
    }

    private List<TheDate> data() {
        Date now = getTime();

        List<TheDate> datas = new ArrayList<>();

        TheDate thd = new TheDate();
        thd.setDate(now);
        thd.setJpDate(now);

        datas.add(thd);
        return datas;
    }

    private Date getTime() {
        // 这里的时间保留保留位数应该和@DateTimeFormat一致，否则值比较时将会不相等
        return new Date();
    }

    private long getTimeSpan(Date from, Date to) {
        return from.getTime() - to.getTime();
    }
    private long getTimeZoneTimeSpan(String timeZoneIdfrom, String timeZoneIdTo) {
        return TimeZone.getTimeZone(timeZoneIdfrom).getRawOffset()
            - TimeZone.getTimeZone(timeZoneIdTo).getRawOffset();
    }

    private void assertListEquals(List<TheDate> listOriginal, List<TheDate> listUsCentral) {
        assertEquals(listOriginal.size(), listUsCentral.size());
        for(int i = 0; i < listOriginal.size(); i ++) {
            TheDate original = listOriginal.get(i);
            TheDate usCentral = listUsCentral.get(i);
            assertEquals(original, usCentral);
        }
    }

    private void assertTimeSpan(List<Date> dateOriginal, List<Date> dateOperated, String timeZoneWrite,
        String timeZoneRead) {

        long timeZoneSpanFromUsCentralToEtcUtc = getTimeZoneTimeSpan(timeZoneWrite, timeZoneRead);

        for(int i = 0; i < dateOriginal.size(); i ++) {
            // 对于同一个时间字符串，A时区 - B时区 = B时区解释 - A时区解释
            long span = getTimeSpan(dateOperated.get(i), dateOriginal.get(i));
            assertEquals(timeZoneSpanFromUsCentralToEtcUtc, span);
        }
    }

    private List<Date> collectDate( final List<TheDate> list, Function<TheDate, Date> function) {
        return list.stream().map(function).collect(Collectors.toList());
    }
}
