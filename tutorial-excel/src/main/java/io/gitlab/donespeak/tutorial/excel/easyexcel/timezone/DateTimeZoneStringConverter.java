package io.gitlab.donespeak.tutorial.excel.easyexcel.timezone;

import com.alibaba.excel.converters.date.DateStringConverter;
import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.property.ExcelContentProperty;

import java.text.ParseException;
import java.util.Date;

/**
 * @author DoneSpeak
 * @date 2019/11/21 22:01
 */
public class DateTimeZoneStringConverter extends DateStringConverter {

    private final String globalTimeZoneId;

    public DateTimeZoneStringConverter() {
        super();
        globalTimeZoneId = null;
    }

    public DateTimeZoneStringConverter(String timeZoneId) {
        super();
        globalTimeZoneId = timeZoneId;
    }

    @Override
    public Date convertToJavaData(CellData cellData, ExcelContentProperty contentProperty,
        GlobalConfiguration globalConfiguration) throws ParseException {

        String timeZoneId = getTimeZoneId(contentProperty);
        String timeFormat = getTimeFormat(contentProperty);

        // System.out.println(String.format("%s: %s: %s", cellData.getStringValue(), timeFormat, timeZoneId));
        Date date = DateUtils.parseDate(cellData.getStringValue(), timeFormat , timeZoneId);
        return date;
    }

    @Override
    public CellData convertToExcelData(Date value, ExcelContentProperty contentProperty,
        GlobalConfiguration globalConfiguration) {

        String timeZoneId = getTimeZoneId(contentProperty);
        String timeFormat = getTimeFormat(contentProperty);

        // System.out.println(String.format("%s: %s: %s", value, timeFormat, timeZoneId));
        String excelValue = DateUtils.format(value, timeFormat, timeZoneId);
        return new CellData(excelValue);
    }

    private String getTimeZoneId(ExcelContentProperty contentProperty) {
        if (contentProperty == null) {
            return null;
        }
        return DateTimeZoneUtil.getTimeZone(contentProperty.getField(), globalTimeZoneId);
    }

    private String getTimeFormat(ExcelContentProperty contentProperty) {
        if (contentProperty == null || contentProperty.getDateTimeFormatProperty() == null) {
            return null;
        }
        return contentProperty.getDateTimeFormatProperty().getFormat();
    }
}
