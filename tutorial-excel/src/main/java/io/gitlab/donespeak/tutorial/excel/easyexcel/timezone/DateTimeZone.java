package io.gitlab.donespeak.tutorial.excel.easyexcel.timezone;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.TimeZone;

/**
 * 指定一个Date属性的时区。
 *
 * 可以确定TimeZoneId的方法：
 * <ul>
 *     <li>https://www.zeitverschiebung.net/cn/all-time-zones.html</li>
 *     <li>https://my.oschina.net/u/2426199/blog/861096</li>
 * </ul>
 *
 * @author DoneSpeak
 * @date 2019/11/21 22:01
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface DateTimeZone {

    /**
     * Specific value reference {@link TimeZone#getAvailableIDs()}
     *
     * @return
     */
    String value() default "";
}
