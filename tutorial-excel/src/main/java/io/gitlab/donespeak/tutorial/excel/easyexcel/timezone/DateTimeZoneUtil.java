package io.gitlab.donespeak.tutorial.excel.easyexcel.timezone;

import com.alibaba.excel.util.StringUtils;

import java.lang.reflect.Field;

/**
 * @author DoneSpeak
 * @date 2019/11/21 22:01
 */
public class DateTimeZoneUtil {

    public static String getTimeZone(Field field, String defaultTimeZoneId) {
        DateTimeZone dateTimeZone = field.getAnnotation(DateTimeZone.class);
        if (dateTimeZone == null) {
            // 如果Field没有DateTimeZone注解，则使用全局的
            return defaultTimeZoneId;
        }
        String timeZoneId = dateTimeZone.value();
        if (StringUtils.isEmpty(timeZoneId)) {
            // 如果Field的DateTimeZone注解的值为空，则使用全局的
            return defaultTimeZoneId;
        }
        return timeZoneId;
    }
}
