package io.gitlab.donespeak.tutorial.excel.data.model;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.format.NumberFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * @author DoneSpeak
 * @date 2019/11/14 11:59
 */

@ContentRowHeight(10)
@HeadRowHeight(20)
@ColumnWidth(25)

@Getter
@Setter
@NoArgsConstructor
@ToString
public class Staff {
    @ExcelProperty(value = "Name", index = 1)
    private String name;
    @ExcelProperty(value = "Email", index = 2)
    private String email;
    @ExcelProperty(value = "Phone", index = 3)
    private String phone;
    @ExcelProperty(value = "Age", index = 4)
    private int age;
    @ExcelProperty(value = "Gender", index = 5)
    private String gender;

    @NumberFormat("#.##")
    @ExcelProperty(value = "Height", index = 6)
    private double height;

    @DateTimeFormat("yyyy年MM月dd日HH时mm分ss秒")
    // @ExcelProperty(value = "Birthday", index = 7)
    private Date birthday;

    @ExcelProperty(value = "Address", index = 8)
    private String address;

    @ExcelProperty(value = "CreatedTime", index = 9)
    private long createdTime;
    @ExcelIgnore
    private long updatedTime;
}
