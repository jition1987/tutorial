package io.gitlab.donespeak.tutorial.excel.easyexcel.timezone;

import com.alibaba.excel.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author DoneSpeak
 * @date 2019/11/21 22:01
 */
public class DateUtils {

    public static final String DATE_FORMAT_10 = "yyyy-MM-dd";
    public static final String DATE_FORMAT_14 = "yyyyMMddHHmmss";
    public static final String DATE_FORMAT_17 = "yyyyMMdd HH:mm:ss";
    public static final String DATE_FORMAT_19 = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_19_FORWARD_SLASH = "yyyy/MM/dd HH:mm:ss";
    private static final String MINUS = "-";

    private DateUtils() {
        throw new AssertionError("DateUtils can't be instantiated.");
    }

    /**
     * convert string to date
     */
    public static Date parseDate(String dateString, String dateFormat, String timeZone) throws ParseException {
        if (StringUtils.isEmpty(dateFormat)) {
            dateFormat = switchDateFormat(dateString);
        }
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        if(!StringUtils.isEmpty(timeZone)) {
            sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
        }
        return sdf.parse(dateString);
    }

    /**
     * convert string to date
     */
    public static Date parseDate(String dateString) throws ParseException {
        return parseDate(dateString, switchDateFormat(dateString), null);
    }

    /**
     * switch date format
     */
    private static String switchDateFormat(String dateString) {
        int length = dateString.length();
        switch (length) {
            case 19:
                if (dateString.contains(MINUS)) {
                    return DATE_FORMAT_19;
                } else {
                    return DATE_FORMAT_19_FORWARD_SLASH;
                }
            case 17:
                return DATE_FORMAT_17;
            case 14:
                return DATE_FORMAT_14;
            case 10:
                return DATE_FORMAT_10;
            default:
                throw new IllegalArgumentException("can not find date format for：" + dateString);
        }
    }

    /**
     * Format date
     * <p>
     * yyyy-MM-dd HH:mm:ss
     */
    public static String format(Date date, String timeZone) {
        return format(date, null, timeZone);
    }

    /**
     * Format date
     *
     * 当dateFormat为空时，默认使用 yyyy-MM-dd HH:mm:ss
     */
    public static String format(Date date, String dateFormat, String timeZone) {
        if (date == null) {
            return "";
        }
        if (StringUtils.isEmpty(dateFormat)) {
            dateFormat = DATE_FORMAT_19;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        if(!StringUtils.isEmpty(timeZone)) {
            sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
        }
        return sdf.format(date);
    }
}
