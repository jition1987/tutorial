package io.gitlab.donespeak.tutorial.excel.data.model;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author DoneSpeak
 * @date 2019/11/14 18:41
 */
@Data
public class Birthday {
    @DateTimeFormat("yyyy-MM-dd hh:mm:ss")
    @ExcelProperty(value = "Birthday")
    private Date date;
    @ExcelProperty(value = "Second")
    private Date second;
    @ExcelProperty(value = "MillSecond")
    private Date millSecond;
}
