package io.gitlab.donespeak.tutorial.excel.data.repository;

import io.gitlab.donespeak.tutorial.excel.data.model.Staff;

/**
 * @author DoneSpeak
 * @date 2019/11/14 12:09
 */
public class StaffRepository extends AbstractRepository<Staff> {
    public StaffRepository() {
        super(Staff.class);
    }
}