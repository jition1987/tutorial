package io.gitlab.donespeak.tutorial.interf;

/**
 * @author DoneSpeak
 */
public interface Person {
    void sayHi();
}
