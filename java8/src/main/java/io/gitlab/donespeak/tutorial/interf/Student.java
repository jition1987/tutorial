package io.gitlab.donespeak.tutorial.interf;

/**
 * @author DoneSpeak
 */
public interface Student extends Person {
    @Override
    default void sayHi() {
        System.out.println("Hi.");
    }
}
