package io.gitlab.donespeak.tutorial.interf;

/**
 * @author DoneSpeak
 */
public interface PartTimeTeacher extends Teacher, Student {
    @Override
    default void sayHi() {
        Teacher.super.sayHi();
    }
}
