package io.gitlab.donespeak.tutorial.interf;

/**
 * @author DoneSpeak
 */
public interface Teacher extends Person {
    @Override
    default void sayHi() {
        System.out.println("Morning.");
    }
}
