package io.gitlab.donespeak.tutorial.itext7.images;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Yang Guanrong
 */
public class AddImage {
    public static final String DEST = "itext7/results/events/jpg_png_svg.pdf";
    public static final String IMAGE_PNG = "itext7/resources/images/horse.png";
    public static final String IMAGE_JPG = "itext7/resources/images/relaxed.jpg";
    public static final String IMAGE_SVG = "itext7/resources/images/symbol.svg";

    public static void main(String[] args) throws IOException, DocumentException {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        new AddImage().createPdf(DEST);
    }

    public class Background extends PdfPageEventHelper {

        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            int pagenumber = writer.getPageNumber();
            if (pagenumber % 2 == 1 && pagenumber != 1) {
                return;
            }
            PdfContentByte canvas = writer.getDirectContentUnder();
            Rectangle rect = document.getPageSize();
            canvas.setColorFill(pagenumber < 3 ? BaseColor.BLUE : BaseColor.LIGHT_GRAY);
            canvas.rectangle(rect.getLeft(), rect.getBottom(), rect.getWidth(), rect.getHeight());
            canvas.fill();
        }
    }

    public void createPdf(String filename) throws IOException, DocumentException {
        // step 1
        Document document = new Document(PageSize.LETTER);
        // step 2
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filename));
        Background event = new Background();
        writer.setPageEvent(event);
        // step 3
        document.open();
        Image jpg = Image.getInstance(IMAGE_PNG);
        jpg.scaleToFit(600, 400);
        // jpg.setAbsolutePosition(0.0f, 0.0f);
        document.add(jpg);

        Image png = Image.getInstance(IMAGE_JPG);
        png.scaleToFit(600, 400);
        // png.setAbsolutePosition(0.0f, 0.0f);
        document.add(png);

        Image svg = Image.getInstance(IMAGE_SVG);
        svg.scaleToFit(600, 400);
        // png.setAbsolutePosition(0.0f, 0.0f);
        document.add(svg);

        // document.newPage();
        // document.add(new Paragraph("An overview"));

        // document.newPage();
        // step 4
        // List<Integer> factors;
        // for (int i = 2; i < 5; i++) {
        //     factors = getFactors(i);
        //     if (factors.size() == 1) {
        //         document.add(new Paragraph("This is a prime number!"));
        //     }
        //     for (int factor : factors) {
        //         document.add(new Paragraph("Factor: " + factor));
        //     }
        //     document.newPage();
        // }
        // step 5
        document.close();
    }

    public static List<Integer> getFactors(int n) {
        List<Integer> factors = new ArrayList<Integer>();
        for (int i = 2; i <= n; i++) {
            while (n % i == 0) {
                factors.add(i);
                n /= i;
            }
        }
        return factors;
    }
}
