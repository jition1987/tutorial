package io.gitlab.donespeak.tutorial.itext7.images;

import com.itextpdf.awt.PdfGraphics2D;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.bridge.BridgeContext;
import org.apache.batik.bridge.DocumentLoader;
import org.apache.batik.bridge.GVTBuilder;
import org.apache.batik.bridge.UserAgent;
import org.apache.batik.bridge.UserAgentAdapter;
import org.apache.batik.gvt.GraphicsNode;
import org.apache.batik.util.XMLResourceDescriptor;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.svg.SVGDocument;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author Yang Guanrong
 */
public class SvgUtil {

    public static void main(String[] args) throws IOException, DocumentException {
        createPdfWithSvg();
    }

    public static void createPdfWithSvg() throws IOException, DocumentException {
        File output = new File("itext7/results/events/svg.pdf");
        File svg = new File("itext7/resources/images/symbol.svg");
        Document document = new Document(PageSize.LETTER);
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(output));

        document.open();
        PdfContentByte canvas = writer.getDirectContent();

        initSvg(svg.getAbsolutePath(), canvas, new int[]{100, 100, 200, 200});

        document.close();
        writer.close();
    }

    public static void initSvg(String url, PdfContentByte canvas, int[] position)
        throws IOException, DocumentException {
        float svgWidth = getSvgWidth(url);
        float svgHeight = getSvgHeight(url);
        int startx = 0;
        int starty = 0;
        float width = svgWidth;
        float height = svgHeight;

        Double picRatio = width/Double.parseDouble(height + StringUtils.EMPTY);
        double picRatioUp = height/Double.parseDouble(width + StringUtils.EMPTY);
        Double actualRatio = position[2]/Double.parseDouble(position[3] + StringUtils.EMPTY);
        if (actualRatio >= picRatio) {
            startx += (position[2] - position[3]*picRatio)/2;
            width = (int)(position[3]*picRatio);
            height = position[3];
        } else {
            starty += (position[3] - position[2]*picRatioUp)/2;
            height = (int)(position[2]*picRatioUp);
            width = position[2];
        }

        //构建svgDocument
        final String parser = XMLResourceDescriptor.getXMLParserClassName();
        SAXSVGDocumentFactory factory = new SAXSVGDocumentFactory(parser);
        UserAgent userAgent = new UserAgentAdapter();
        DocumentLoader loader = new DocumentLoader(userAgent);
        BridgeContext ctx = new BridgeContext(userAgent, loader);
        ctx.setDynamicState(BridgeContext.STATIC);
        GVTBuilder builder = new GVTBuilder();

        PdfTemplate template = canvas.createTemplate(svgWidth, svgHeight);
        //生成awt Graphics2D
        Graphics2D g2d = new PdfGraphics2D(template, svgWidth, svgHeight);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        SVGDocument svgDocument = factory.createSVGDocument(url);
        GraphicsNode graphNode = builder.build(ctx, svgDocument);
        //画svg到画布
        graphNode.paint(g2d);
        g2d.dispose();
        //生成Img
        com.itextpdf.text.Image image = com.itextpdf.text.Image.getInstance(template);
        image.setAbsolutePosition(position[0] + startx, position[1] + starty);
        image.scaleAbsolute(width, height);
        //添加图片到pdf Document
        canvas.addImage(image);
    }

    public static Integer getSvgWidth(String svgUrl) throws IOException {
        String width = getSvgViewBox(svgUrl).split(" ")[2];
        return (int)Double.parseDouble(width);
    }

    public static Integer getSvgHeight(String svgUrl) throws IOException {
        String height = getSvgViewBox(svgUrl).split(" ")[3];
        return (int)Double.parseDouble(height);
    }

    public static String getSvgViewBox(String svgUrl) throws IOException {
        File file = new File(svgUrl);
        String parser = XMLResourceDescriptor.getXMLParserClassName();
        SAXSVGDocumentFactory f = new SAXSVGDocumentFactory(parser);
        org.w3c.dom.Document doc = f.createDocument(file.toURI().toString());
        org.w3c.dom.Element element = doc.getDocumentElement();
        //view中包含这宽高信息
        String box = element.getAttribute("viewBox");
        if(StringUtils.isBlank(box)) {
            return String.join(" ", "0", "0", element.getAttribute("width"), element.getAttribute("height"));
        }
        return box;
    }

}
