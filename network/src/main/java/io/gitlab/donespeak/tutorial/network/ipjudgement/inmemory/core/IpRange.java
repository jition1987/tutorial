package io.gitlab.donespeak.tutorial.network.ipjudgement.inmemory.core;

import lombok.Data;

/**
 * @author Yang Guanrong
 * @date 2020/03/28 15:08
 */
@Data
public class IpRange {
	private String start;
	private String end;
}
