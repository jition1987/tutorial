package io.gitlab.donespeak.tutorial.network.ipjudgement.inmemory;

import io.gitlab.donespeak.tutorial.network.ipjudgement.inmemory.core.IpJudgement;
import io.gitlab.donespeak.tutorial.network.ipjudgement.inmemory.core.IpRange;
import io.gitlab.donespeak.tutorial.network.ipjudgement.util.IpConverter;

import java.util.List;

/**
 * @author Yang Guanrong
 * @date 2020/03/28 15:25
 */
public class IpJudgementInMemory implements IpJudgement {

	private int[][] ipIntRanges;

	public IpJudgementInMemory(List<IpRange> ipRanges) {
		if(ipRanges == null) {
			throw new IllegalArgumentException("The param ipRanges shouldn't be null.");
		}
		this.ipIntRanges = convertIpRanges(ipRanges);
	}

	@Override
	public boolean isIncluded(String ip) {
		return getIncluded(ip) != null;
	}

	@Override
	public IpRange getIncluded(String ip) {
		int ipInt = IpConverter.toInt(ip);
		int[] result = search(ipInt, ipIntRanges);
		return convert(result);
	}

	private int[][] convertIpRanges(List<IpRange> ipRanges) {
		return null;
	}

	private int[] convert(IpRange ipRange) {
		return null;
	}

	private IpRange convert(int[] ipRange) {
		return null;
	}

	private int[] search(int ip, int[][] ipIntRanges) {
		return null;
	}
}
