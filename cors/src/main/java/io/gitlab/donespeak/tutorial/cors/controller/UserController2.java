package io.gitlab.donespeak.tutorial.cors.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.donespeak.tutorial.cors.entity.User;

/**
 * @date 2019/12/02 21:13
 */
@CrossOrigin(origins = {})
@RestController
@RequestMapping("/2/users")
public class UserController2 {

    @GetMapping("/{userId:\\d+}")
    public User getUser(@PathVariable("userId") long userId) {
       User user = new User();
       user.setUserId(userId);
       return user;
    }
}
