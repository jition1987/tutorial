package io.gitlab.donespeak.tutorial.cors.config.properties;

import org.springframework.util.StringUtils;

import lombok.Data;

/**
 * @author Yang Guanrong
 * @date 2020/01/27 15:27
 */
@Data
public class CorsProperties {
    // Ant-style path patterns
    private String[] pathPatterns;
    private String[] allowedOrigins;
    private String[] allowedMethods;
    private String[] allowedHeaders;
    private Boolean allowedCredentials;
    private String[] exposedHeaders;
    private Long maxAge;

    public void setPathPatterns(String[] pathPatterns) {
        this.pathPatterns = pathPatterns;
    }

    public void setPathPatterns(String pathPatterns) {
        this.pathPatterns = StringUtils.split(pathPatterns, ",");
    }

    public void setAllowedOrigins(String[] allowedOrigins) {
        this.allowedOrigins = allowedOrigins;
    }

    public void setAllowedOrigins(String allowedOrigins) {
        this.allowedOrigins = StringUtils.split(allowedOrigins, ",");
    }

    public void setAllowedMethods(String[] allowedMethods) {
        this.allowedMethods = allowedMethods;
    }

    public void setAllowedMethods(String allowedMethods) {
        this.allowedMethods = StringUtils.split(allowedMethods, ",");
    }

    public void setAllowedHeaders(String[] allowedHeaders) {
        this.allowedHeaders = allowedHeaders;
    }

    public void setAllowedHeaders(String allowedHeaders) {
        this.allowedHeaders = StringUtils.split(allowedHeaders, ",");
    }

    public void setExposedHeaders(String[] exposedHeaders) {
        this.exposedHeaders = exposedHeaders;
    }

    public void setExposedHeaders(String exposedHeaders) {
        this.exposedHeaders = StringUtils.split(exposedHeaders, ",");
    }
}