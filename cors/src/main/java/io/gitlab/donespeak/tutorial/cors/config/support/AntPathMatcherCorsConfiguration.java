package io.gitlab.donespeak.tutorial.cors.config.support;

import org.springframework.lang.Nullable;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.ObjectUtils;
import org.springframework.util.PathMatcher;
import org.springframework.util.StringUtils;
import org.springframework.web.cors.CorsConfiguration;

/**
 * @date 2019/12/03 00:04
 */
public class AntPathMatcherCorsConfiguration extends CorsConfiguration {

    private PathMatcher pathMatcher = new AntPathMatcher();

    @Nullable
    @Override
    public String checkOrigin(@Nullable String requestOrigin) {
        System.out.println(requestOrigin);
        if (!StringUtils.hasText(requestOrigin)) {
            return null;
        }
        if (ObjectUtils.isEmpty(this.getAllowedOrigins())) {
            return null;
        }

        if (this.getAllowedOrigins().contains(ALL)) {
            if (!Boolean.TRUE.equals(this.getAllowCredentials())) {
                // ALL 和 TRUE不是不能同时出现吗？
                return ALL;
            }
            else {
                return requestOrigin;
            }
        }

        String lowcaseRequestOrigin = requestOrigin.toLowerCase();
        for (String allowedOrigin : this.getAllowedOrigins()) {
            System.out.println(allowedOrigin + ": " + pathMatcher.match(allowedOrigin.toLowerCase(), lowcaseRequestOrigin));
            if (pathMatcher.match(allowedOrigin.toLowerCase(), lowcaseRequestOrigin)) {
                return requestOrigin;
            }
        }
        return null;
    }
}
