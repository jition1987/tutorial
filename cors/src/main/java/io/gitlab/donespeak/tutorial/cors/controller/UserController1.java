package io.gitlab.donespeak.tutorial.cors.controller;

import io.gitlab.donespeak.tutorial.cors.config.properties.CorsPropertiesList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.donespeak.tutorial.cors.entity.User;

import javax.annotation.PostConstruct;

/**
 * @date 2019/12/02 21:13
 */
@RestController
@RequestMapping("/1/users")
public class UserController1 {

    @Autowired
    private CorsPropertiesList corsPropertiesList;

    @GetMapping("/{userId:\\d+}")
    public User getUser(@PathVariable("userId") long userId) {
       User user = new User();
       user.setUserId(userId);
       return user;
    }

    @PostConstruct
    public void temp() {
        System.out.println(corsPropertiesList);
    }
}
