package io.gitlab.donespeak.tutorial.cors.controller;

import io.gitlab.donespeak.tutorial.cors.entity.User;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @date 2019/12/02 21:12
 */
@RestController
@RequestMapping("/3/users")
public class UserController3 {

    private static final String MY_IP = "http://192.168.1.106:8080";
    private static final String NOT_MY_IP = "http://192.168.1.121:8080";

    @CrossOrigin(origins = {NOT_MY_IP})
    @GetMapping("/{userId:\\d+}/not")
    public User getUserNotMyIp(@PathVariable long userId) {
        return newUser(userId);
    }

    @CrossOrigin(origins = {MY_IP})
    @GetMapping("/{userId:\\d+}/my")
    public User getUserMyIp(@PathVariable long userId, HttpServletRequest request) {
        String username = request.getHeader("username");
        User user = newUser(userId);
        user.setName(username);
        return user;
    }

    @CrossOrigin(origins = {MY_IP}, allowedHeaders = "username")
    @GetMapping("/{userId:\\d+}/headers")
    public User getUserWithHeader(@PathVariable long userId, HttpServletRequest request) {
        String username = request.getHeader("username");
        User user = newUser(userId);
        user.setName(username);
        return user;
    }

    private User newUser(long userId) {
        User user = new User();
        user.setUserId(userId);
        user.setName("xiaoming");
        user.setAge(12);
        return user;
    }
}
