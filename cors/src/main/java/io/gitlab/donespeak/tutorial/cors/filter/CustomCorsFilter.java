package io.gitlab.donespeak.tutorial.cors.filter;

import io.gitlab.donespeak.tutorial.cors.config.properties.CorsProperties;
import io.gitlab.donespeak.tutorial.cors.config.properties.CorsPropertiesList;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.StringUtils;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class CustomCorsFilter implements Filter {

    private CorsPropertiesList corsPropertiesList;
    private AntPathMatcher antPathMatcher = new AntPathMatcher();
    private static final String ALL = "*";

    private Map<String, CorsProperties> corsPropertiesMap = new LinkedHashMap<>();

    public CustomCorsFilter(CorsPropertiesList corsPropertiesList) {
        this.corsPropertiesList = corsPropertiesList;
        for(CorsProperties corsProperties: corsPropertiesList.getList()) {
            for(String pathPattern: corsProperties.getPathPatterns()) {
                corsPropertiesMap.put(pathPattern, corsProperties);
            }
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException {
        HttpServletRequest servletRequest = (HttpServletRequest)request;
        HttpServletResponse servletResponse = (HttpServletResponse)response;

        String origin = servletRequest.getHeader("Origin");
        List<CorsProperties> corsPropertiesList = getCorsPropertiesMatch(servletRequest.getServletPath());
        if(log.isDebugEnabled()) {
            log.debug("Try to check origin: " + origin);
        }
        CorsProperties originPassCorsProperties = null;
        for(CorsProperties corsProperties: corsPropertiesList) {
            if (corsProperties != null && isOriginAllowed(origin, corsProperties.getAllowedOrigins())) {
                originPassCorsProperties = corsProperties;
                break;
            }
        }
        if (originPassCorsProperties != null) {
            servletResponse.setHeader("Access-Control-Allow-Origin", origin);
            servletResponse.setHeader("Access-Control-Allow-Methods",
                StringUtils.arrayToCommaDelimitedString(originPassCorsProperties.getAllowedMethods()));
            servletResponse.setHeader("Access-Control-Allow-Headers",
                StringUtils.arrayToCommaDelimitedString(originPassCorsProperties.getAllowedHeaders()));
            servletResponse.addHeader("Access-Control-Expose-Headers",
                StringUtils.arrayToCommaDelimitedString(originPassCorsProperties.getExposedHeaders()));
            servletResponse.addHeader("Access-Control-Allow-Credentials",
                String.valueOf(originPassCorsProperties.getAllowedCredentials()));
            servletResponse.setHeader("Access-Control-Max-Age", String.valueOf(originPassCorsProperties.getMaxAge()));
        } else {
            servletResponse.setHeader("Access-Control-Allow-Origin", null);
        }

        if ("OPTIONS".equals(servletRequest.getMethod())) {
            servletResponse.setStatus(HttpServletResponse.SC_OK);
        } else {
            chain.doFilter(servletRequest, servletResponse);
        }
    }

    private List<CorsProperties> getCorsPropertiesMatch(String path) {
        List<CorsProperties> corsPropertiesList = new ArrayList<>();
       for(Map.Entry<String, CorsProperties> entry: corsPropertiesMap.entrySet()) {
           if(antPathMatcher.match(entry.getKey(), path)) {
               corsPropertiesList.add(entry.getValue());
           }
       }
       return corsPropertiesList;
    }

    private boolean isOriginAllowed(String origin, String[] allowedOrigins) {
        if (StringUtils.isEmpty(origin) || (allowedOrigins == null || allowedOrigins.length == 0)) {
            return false;
        }
        for (String allowedOrigin : allowedOrigins) {
            if (ALL.equals(allowedOrigin) || isOriginMatch(origin, allowedOrigin)) {
                return true;
            }
        }
        return false;
    }

    private boolean isOriginMatch(String origin, String originPattern) {
        return antPathMatcher.match(originPattern, origin);
    }
}