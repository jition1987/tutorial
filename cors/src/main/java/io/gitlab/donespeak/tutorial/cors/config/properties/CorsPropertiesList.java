package io.gitlab.donespeak.tutorial.cors.config.properties;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author Yang Guanrong
 * @date 2020/01/27 12:57
 */
@Data
@NoArgsConstructor
@Component
@ConfigurationProperties("corses")
public class CorsPropertiesList {

    private List<CorsProperties> list;

}