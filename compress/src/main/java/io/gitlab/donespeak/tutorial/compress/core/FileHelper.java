package io.gitlab.donespeak.tutorial.compress.core;

import java.io.File;

/**
 * @author DoneSpeak
 */
public class FileHelper {

    protected String source;

    protected String target;

    public void source(String source) {
        this.source = source;
    }

    public void target(String target) {
        this.target = target;
    }

    protected File sourceFile() {
        return new File(source);
    }

    protected File targetFile() {
        return new File(target);
    }

}
