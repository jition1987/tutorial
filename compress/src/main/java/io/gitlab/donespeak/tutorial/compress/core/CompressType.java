package io.gitlab.donespeak.tutorial.compress.core;

/**
 * @author DoneSpeak
 */
public enum CompressType {
    /**
     * zip
     */
    ZIP,
    /**
     * tar
     */
    TAR,
    /**
     * gz
     */
    GZ,
    /**
     * 7z
     */
    SEVEN_ZIP
    ;
}
