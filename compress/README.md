# 压缩

## 功能列表

压缩功能

- [ ] 将一个文件/目录压缩
- [ ] 将一个文件列表压缩
- [ ] 将一个文件压缩到到一个压缩包中

解压功能

- [ ] 解压一个压缩包

信息获取

- [ ] 获取压缩包中的文件列表



