PDF
===

## 工具

- [pdfresizer.com](https://pdfresizer.com/resize)

## 提取图片

stackoverflow上的问题：
- [How to decode image with /ASCIIHexDecode](https://stackoverflow.com/questions/47101222/how-to-decode-image-with-asciihexdecode)

开源代码:

- [mkl-public/testarea-itext5/.../mageExtraction.java](https://github.com/mkl-public/testarea-itext5/blob/master/src/test/java/mkl/testarea/itext5/extract/ImageExtraction.java) 一些itext5对stackoverflow问题的代码
- [itext/i7js-examples](https://github.com/itext/i7js-examples/tree/develop/cmpfiles/sandbox) itext7的样例代码

图片操作相关的案例：[itext/i7js-examples/.../images/](https://github.com/itext/i7js-examples/tree/develop/src/main/java/com/itextpdf/samples/sandbox/images)

## 减小PDF的文件大小

- [itext/i7js-examples/.../ReduceSize.java](https://github.com/itext/i7js-examples/blob/develop/src/main/java/com/itextpdf/samples/sandbox/images/ReduceSize.java)

## 替换图片

- [ ] png 图片背景黑化问题  
    - 转白色背景：https://my.oschina.net/itll/blog/1601028
    - 

使用有透明度的ImageType，但没有生效
```java
Builder<BufferedImage> builder = Thumbnails.of(sourceImage)
    .imageType(BufferedImage.TYPE_INT_ARGB)
    .forceSize(width, height);
builder.outputFormat("png").toFile(destFile);
```

## 和pdf相关的开源项目

### zsc19921016/pdf_converter

[zsc19921016/pdf_converter](https://github.com/zsc19921016/pdf_converter).  

converting txt,word,excel,ppt,image and compress package to PDF file

## 待处理

- [ ] 案例测试：https://www.tutorialkart.com/pdfbox/how-to-get-location-and-size-of-images-in-pdf/

## 需要跟进

- [Replace Inline Images in PDF with pdfbox](https://stackoverflow.com/questions/54744038/replace-inline-images-in-pdf-with-pdfbox)

## 参考

- [PDF32000_2008.pdf @adobe.com](https://www.adobe.com/content/dam/acom/en/devnet/acrobat/pdfs/PDF32000_2008.pdf)
- [https://www.tutorialkart.com/pdfbox](https://www.tutorialkart.com/pdfbox/how-to-get-location-and-size-of-images-in-pdf/)
- [使用pdfBox实现pdf转图片，解决中文方块乱码等问题](https://www.cnblogs.com/hujunzheng/p/10508044.html)