Shrink
==

来源：[bnanes/shrink-pdf @Github](https://github.com/bnanes/shrink-pdf)  

测试效果：

6.8MB -> 2.4MB
- JPG: 模糊
- PNG: 透明背景变黑色背景，非透明部分颜色全部颜色发生改变
- 有些页会整体变成白色

实现原理：

抽取每一页的图片出来，然后进行图片压缩。