package io.gitlab.donespeak.tutorial.pdf.reducesize.dpi;

import io.gitlab.donespeak.tutorial.pdf.reducesize.imagecompress.ThumbnailatorCompressor;
import org.apache.pdfbox.contentstream.PDFStreamEngine;
import org.apache.pdfbox.contentstream.operator.DrawObject;
import org.apache.pdfbox.contentstream.operator.Operator;
import org.apache.pdfbox.contentstream.operator.state.Concatenate;
import org.apache.pdfbox.contentstream.operator.state.Restore;
import org.apache.pdfbox.contentstream.operator.state.Save;
import org.apache.pdfbox.contentstream.operator.state.SetGraphicsStateParameters;
import org.apache.pdfbox.contentstream.operator.state.SetMatrix;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.form.PDFormXObject;
import org.apache.pdfbox.pdmodel.graphics.image.JPEGFactory;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.util.Matrix;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * TODO Replace Inline Images in PDF with pdfbox:
 * - https://stackoverflow.com/questions/54744038/replace-inline-images-in-pdf-with-pdfbox
 * - PDInlineImageTest.java: https://www.javatips.net/api/pdfbox-master/pdfbox-trunk/pdfbox/src/test/java/org/apache/pdfbox/pdmodel/graphics/image/PDInlineImageTest.java
 * - resize inline images: https://lists.apache.org/thread.html/3655151dcd2f3156e4177ad21eecf42bef9081bdba9b0d5a69598003@%3Cusers.pdfbox.apache.org%3E
 *
 * @author Yang Guanrong
 */
public class DpiReduceWithEngine {

    public static void reduce(File pdf, int dpi, File output) throws IOException {
        PDDocument document = null;
        try {
            document = PDDocument.load(pdf);
            ReduceImageEngine reduceImageEngine = new ReduceImageEngine(document, dpi);
            int pageNum = 0;
            for (PDPage page : document.getPages()) {
                pageNum++;
                System.out.println("Processing page: " + pageNum);
                reduceImageEngine.processPage(page);
            }
            document.save(output);
        } finally {
            if (document != null) {
                document.close();
            }
        }
    }

    private static class ReduceImageEngine extends PDFStreamEngine {

        private int dpi;
        private PDDocument document;

        private ThumbnailatorCompressor compressor = new ThumbnailatorCompressor();

        public ReduceImageEngine(PDDocument document, int dpi) {
            this.dpi = dpi;
            this.document = document;
            prepare();
        }

        private void prepare() {
            // preparing PDFStreamEngine
            addOperator(new Concatenate());
            addOperator(new DrawObject());
            addOperator(new SetGraphicsStateParameters());
            addOperator(new Save());
            addOperator(new Restore());
            addOperator(new SetMatrix());
        }

        @Override
        protected void processOperator(Operator operator, List<COSBase> operands) throws IOException {
            String operation = operator.getName();
            if ("Do".equals(operation)) {
                COSName objectName = (COSName)operands.get(0);
                // get the PDF object
                PDXObject xobject = getResources().getXObject(objectName);
                // check if the object is an image object
                if (xobject instanceof PDImageXObject) {
                    PDImageXObject image = (PDImageXObject)xobject;
                    int imageWidth = image.getWidth();
                    int imageHeight = image.getHeight();
                    StringBuilder sb = new StringBuilder();
                    sb.append("\nImage [" + objectName.getName() + "]:" + image + "\n");
                    sb.append("ColorSpace: " + image.getColorSpace() + "\n");
                    sb.append("Stencil: " + image.isStencil() + "\n");
                    sb.append("Suffix: " + image.getSuffix() + "\n");
                    sb.append("Decode: " + image.getDecode() + "\n");
                    sb.append("Filters: " + image.getStream().getFilters() + "\n");

                    System.out.println(sb.toString());

                    Matrix ctmNew = getGraphicsState().getCurrentTransformationMatrix();
                    float imgWidthInUserSpaceUnit = ctmNew.getScalingFactorX();

                    // 以左下角为原点构建坐标系，(x, y) => (右，上）
                    // position of image in the pdf in terms of user space units
                    System.out.println("position in PDF = " + ctmNew.getTranslateX() + ", " + ctmNew.getTranslateY()
                        + " in user space units");
                    // raw size in pixels
                    System.out.println("raw image size  = " + imageWidth + ", " + imageHeight + " in pixels");
                    // displayed size in user space units
                    System.out.println("displayed size  = " + imgWidthInUserSpaceUnit + ", " + imgWidthInUserSpaceUnit + " in user space units");

                    double scale = calculateScale(imgWidthInUserSpaceUnit, imageWidth, dpi);
                    if(0 < scale && scale < 1) {
                        PDImageXObject imgNew = scaleImage(document, image, 0.5, scale);
                        getResources().put(objectName, imgNew);
                    }
                } else if (xobject instanceof PDFormXObject) {
                    PDFormXObject form = (PDFormXObject)xobject;
                    showForm(form);
                }
            } else {
                super.processOperator(operator, operands);
            }
        }

        private double calculateScale(float userSpaceUnitLength, int imageWidthPixel, int dpi) {
            double imageScaledX = calculateTargetPixel(userSpaceUnitLength, dpi);
            double scale = imageScaledX / imageWidthPixel;
            return scale;
        }

        private double calculateTargetPixel(float userSpaceUnitLength, int dpi) {
            // position in user space units. 1 unit = 1/72 inch at 72 dpi
            final int DEFAULT_USER_SPACE_UNIT = 72;
            return userSpaceUnitLength / DEFAULT_USER_SPACE_UNIT * dpi;
        }

        private PDImageXObject scaleImage(PDDocument document, PDImageXObject img, double quality, double scale)
            throws IOException {
            BufferedImage bufferedImage = compressor.compress(img.getImage(), img.getSuffix(), quality, scale);
            PDImageXObject imgNew = null;
            System.out.println("img(w, h): (" + img.getWidth() + "," + img.getHeight() + ")");
            System.out.println("bufferedImage(w, h): (" + bufferedImage.getWidth() + "," + bufferedImage.getHeight() + ")");
            if("png".equalsIgnoreCase(img.getSuffix())) {
                imgNew = LosslessFactory.createFromImage(document, bufferedImage);
            } else {
                imgNew = JPEGFactory.createFromImage(document, bufferedImage);
            }
            return imgNew;
        }
    }
}
