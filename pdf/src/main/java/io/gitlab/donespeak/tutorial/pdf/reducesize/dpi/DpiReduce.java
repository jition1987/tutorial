package io.gitlab.donespeak.tutorial.pdf.reducesize.dpi;

import net.coobird.thumbnailator.Thumbnails;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.image.JPEGFactory;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * @author Yang Guanrong
 */
public class DpiReduce {

    public static enum DPI {
        /**
         * low quality (72 dpi images) for screen viewing;
         */
        LOW(72),
        /**
         * medium quality (150 dpi images) for eBooks;
         */
        MEDIUM(150),
        /**
         * high quality (300 dpi images) for printing;
         */
        HIGH(300),
        /**
         * very high quality (300 dpi images, preserving all colors) for high quality and resolution prints;
         */
        // VERY_HIGH(Integer.MAX_VALUE)
        ;
        private final int value;

        DPI(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    /**
     * 该实现方法是获取每一页中的resouce进行独立处理的，也就是说，如果所有的资源都是同一个资源的引用，那么经过这个处理之后，
     * 每个引用都会转化为一个stream对象，从而变得更大。
     *
     * @param input
     * @param output
     * @throws IOException
     */
    public static void compress(File input, DPI dpi, File output) throws IOException {
        if(!output.getParentFile().exists()) {
            output.getParentFile().mkdirs();
        }
        PDDocument document = PDDocument.load(input);
        int pageIndex = 1;
        PDDocumentCatalog catalog = document.getDocumentCatalog();

        for (PDPage page : catalog.getPages()) {
            PDResources pdResources = page.getResources();
            for (COSName c : pdResources.getXObjectNames()) {
                System.out.println("PDResources[" + pageIndex + "]#COSName: " + c.getName());
                PDXObject o = pdResources.getXObject(c);
                System.out.println("PDResources[" + pageIndex + "]#PDXObject: " + o.toString());
                // TODO 还需要提取 PDFormXObject 中的图片
                // https://github.com/mkl-public/testarea-itext5/blob/master/src/test/java/mkl/testarea/itext5/extract/ImageExtraction.java
                if (o instanceof PDImageXObject) {
                    // 看下是否可以做到过滤不压缩
                    PDImageXObject img = (PDImageXObject) o;
                    PDImageXObject imgNew = compress(document, img, dpi);
                    pdResources.put(c, imgNew);
                }
            }
            pageIndex ++;
        }
        if(!output.getParentFile().exists()) {
            output.getParentFile().mkdirs();
        }
        document.save(output);
        document.close();
    }

    private static PDImageXObject compress(PDDocument document, PDImageXObject img, DPI dpi) throws IOException {
        PDImageXObject imgNew = null;
        // TODO 需要设法取得图片 user space units 值
        BufferedImage bufferedImage = compress(img.getImage(), img.getSuffix(), dpi);
        System.out.println("img(w, h): (" + img.getWidth() + "," + img.getHeight() + ")");
        System.out.println("bufferedImage(w, h): (" + bufferedImage.getWidth() + "," + bufferedImage.getHeight() + ")");
        if("png".equalsIgnoreCase(img.getSuffix())) {
            imgNew = LosslessFactory.createFromImage(document, bufferedImage);
        } else {
            imgNew = JPEGFactory.createFromImage(document, bufferedImage);
        }
        return imgNew;
    }

    private static BufferedImage compress(BufferedImage image, String format, DPI dpi) throws IOException {

        BufferedImage thumbnail = Thumbnails.of(image)
            .imageType(image.getType())
            // .scale(scale)
            // .outputQuality(quality)
            // .outputFormat(imageFormat)
            .useOriginalFormat()
            .asBufferedImage();

        return thumbnail;
    }

}
