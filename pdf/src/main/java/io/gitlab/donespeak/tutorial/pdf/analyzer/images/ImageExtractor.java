package io.gitlab.donespeak.tutorial.pdf.analyzer.images;

import com.itextpdf.text.log.Logger;
import com.itextpdf.text.log.LoggerFactory;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * PDF 图片提取
 *
 * @author Yang Guanrong
 */
public class ImageExtractor implements ImageExtract {

    private static final Logger log = LoggerFactory.getLogger(ImageExtractor.class);

    /**
     * 提取PDF中的图片
     *
     * @param pdf PDF源文件
     * @param imageDir 图片保存目录
     */
    @Override
    public void extract(File pdf, File imageDir) throws IOException {
        if(!imageDir.exists()) {
            imageDir.mkdirs();
        }
        PDDocument document = PDDocument.load(pdf);
        PDPageTree list = document.getPages();
        System.out.println("PDPageTree#count: " + list.getCount());
        int pageIndex = 1;
        for (PDPage page : list) {
            PDResources pdResources = page.getResources();
            System.out.println(pdResources.toString());
            for (COSName c : pdResources.getXObjectNames()) {
                System.out.println("PDResources[" + pageIndex + "]#COSName: " + c.getName());
                PDXObject o = pdResources.getXObject(c);
                System.out.println("PDResources[" + pageIndex + "]#PDXObject: " + o.toString());
                // TODO 还需要提取 PDFormXObject 中的图片
                // https://github.com/mkl-public/testarea-itext5/blob/master/src/test/java/mkl/testarea/itext5/extract/ImageExtraction.java
                if (o instanceof PDImageXObject) {
                    PDImageXObject img = (PDImageXObject) o;
                    File file = new File(imageDir, pageIndex + "-" + System.nanoTime() + "." + img.getSuffix());
                    ImageIO.write(((PDImageXObject)o).getImage(), img.getSuffix(), file);
                }
            }
            pageIndex ++;
        }
        log.info("Images have been extracted successfully! Check your images folder.");
    }

    @Override
    public void extract(InputStream pdf, File imageDir) throws IOException {

    }
}
