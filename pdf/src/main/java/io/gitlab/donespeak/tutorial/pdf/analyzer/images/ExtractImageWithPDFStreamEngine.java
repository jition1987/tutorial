package io.gitlab.donespeak.tutorial.pdf.analyzer.images;

import org.apache.pdfbox.contentstream.PDFStreamEngine;
import org.apache.pdfbox.contentstream.operator.Operator;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.form.PDFormXObject;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * 参考: https://www.tutorialkart.com/pdfbox/extract-images-from-pdf-using-pdfbox/
 *
 * @author Yang Guanrong
 */
public class ExtractImageWithPDFStreamEngine {

    /**
     * 提取每一页的图片，如果有多张图片均引用同一张图片，则每个引用提取为一张图片。
     */
    public static void extract(File pdf, File imageDir) throws IOException {
        PDDocument document = null;
        try {
            document = PDDocument.load(pdf);
            ImageExtractPDFStreamEngine printer = new ImageExtractPDFStreamEngine(imageDir);
            int pageNum = 0;
            for (PDPage page : document.getPages()) {
                pageNum++;
                System.out.println("Processing page: " + pageNum);
                printer.processPage(page);
            }
        } finally {
            if (document != null) {
                document.close();
            }
        }
    }

    private static class ImageExtractPDFStreamEngine extends PDFStreamEngine {

        public int imageNumber = 1;

        private File imageDir;

        public ImageExtractPDFStreamEngine(File imageDir) {
            this.imageDir = imageDir;
        }

        /**
         * @param operator
         *            The operation to perform.
         * @param operands
         *            The list of arguments.
         *
         * @throws IOException
         *             If there is an error processing the operation.
         */
        @Override
        protected void processOperator(Operator operator, List<COSBase> operands) throws IOException {
            String operation = operator.getName();
            System.out.println("operation: " + operation);
            if ("Do".equals(operation)) {
                COSName objectName = (COSName)operands.get(0);
                PDXObject xobject = getResources().getXObject(objectName);
                if (xobject instanceof PDImageXObject) {
                    PDImageXObject image = (PDImageXObject)xobject;
                    int imageWidth = image.getWidth();
                    int imageHeight = image.getHeight();

                    // same image to local
                    BufferedImage bImage = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB);
                    bImage = image.getImage();
                    ImageIO.write(bImage, image.getSuffix(), new File(imageDir, "image_" + imageNumber + "." + image.getSuffix()));
                    System.out.println(operation + ": Image saved.");
                    imageNumber++;

                } else if (xobject instanceof PDFormXObject) {
                    PDFormXObject form = (PDFormXObject)xobject;
                    showForm(form);
                }
            } else {
                super.processOperator(operator, operands);
            }
        }

    }

}