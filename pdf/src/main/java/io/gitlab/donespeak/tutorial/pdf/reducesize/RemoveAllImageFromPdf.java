package io.gitlab.donespeak.tutorial.pdf.reducesize;

import io.gitlab.donespeak.tutorial.pdf.reducesize.imagecompress.ThumbnailatorCompressor;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.image.JPEGFactory;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * @author Yang Guanrong
 */
public class RemoveAllImageFromPdf {

    public static void extractImages(File input, File imageDir) throws IOException {
        if(imageDir.exists()) {
            imageDir.delete();
        }
        imageDir.mkdirs();
        PDDocument document = PDDocument.load(input);
        int pageIndex = 1;
        PDDocumentCatalog catalog = document.getDocumentCatalog();
        for (PDPage page : catalog.getPages()) {
            PDResources pdResources = page.getResources();
            System.out.println(pdResources.toString());
            for (COSName c : pdResources.getXObjectNames()) {
                System.out.println("PDResources[" + pageIndex + "]#COSName: " + c.getName());
                PDXObject o = pdResources.getXObject(c);
                System.out.println("PDResources[" + pageIndex + "]#PDXObject: " + o.toString());
                // TODO 还需要提取 PDFormXObject 中的图片
                // https://github.com/mkl-public/testarea-itext5/blob/master/src/test/java/mkl/testarea/itext5/extract/ImageExtraction.java
                if (o instanceof PDImageXObject) {
                    PDImageXObject img = (PDImageXObject) o;
                    System.out.println(img.getSuffix() + "-" + img.getBitsPerComponent() + "-" + img.getColorSpace());
                    File file = new File(imageDir, pageIndex + "-" + c.getName() + "-" + img.getColorSpace() + "-" + System.nanoTime() + "." + img.getSuffix());
                    ImageIO.write(((PDImageXObject)o).getImage(), img.getSuffix(), file);
                }
            }
            pageIndex ++;
        }
        // document.save(output);
        // 抛出异常，应该在finally中close
        document.close();
    }

    /**
     * 该实现方法是获取每一页中的resouce进行独立处理的，也就是说，如果所有的资源都是同一个资源的引用，那么经过这个处理之后，
     * 每个引用都会转化为一个stream对象，从而变得更大。
     *
     * @param input
     * @param output
     * @throws IOException
     */
    public static void compress(File input, File output) throws IOException {
        if(!output.getParentFile().exists()) {
            output.getParentFile().mkdirs();
        }
        ThumbnailatorCompressor compressor = new ThumbnailatorCompressor();
        PDDocument document = PDDocument.load(input);
        int pageIndex = 1;
        PDDocumentCatalog catalog = document.getDocumentCatalog();

        for (PDPage page : catalog.getPages()) {
            PDResources pdResources = page.getResources();
            for (COSName c : pdResources.getXObjectNames()) {
                System.out.println("PDResources[" + pageIndex + "]#COSName: " + c.getName());
                PDXObject o = pdResources.getXObject(c);
                System.out.println("PDResources[" + pageIndex + "]#PDXObject: " + o.toString());
                // TODO 还需要提取 PDFormXObject 中的图片
                // https://github.com/mkl-public/testarea-itext5/blob/master/src/test/java/mkl/testarea/itext5/extract/ImageExtraction.java
                if (o instanceof PDImageXObject) {
                    // 看下是否可以做到过滤不压缩
                    PDImageXObject img = (PDImageXObject) o;
                    BufferedImage bufferedImage = compressor.compress(img.getImage(), img.getSuffix(), 0.8, 0.5);
                    PDImageXObject imgNew = null;
                    System.out.println("img(w, h): (" + img.getWidth() + "," + img.getHeight() + ")");
                    System.out.println("bufferedImage(w, h): (" + bufferedImage.getWidth() + "," + bufferedImage.getHeight() + ")");
                    if("png".equalsIgnoreCase(img.getSuffix())) {
                        imgNew = LosslessFactory.createFromImage(document, bufferedImage);
                    } else {
                        imgNew = JPEGFactory.createFromImage(document, bufferedImage);
                    }
                    pdResources.put(c, imgNew);
                }
            }
            pageIndex ++;
        }
        if(!output.getParentFile().exists()) {
            output.getParentFile().mkdirs();
        }
        document.save(output);
        document.close();
    }
}
