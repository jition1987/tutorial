package io.gitlab.donespeak.tutorial.pdf.reducesize.itext;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PRStream;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfStream;
import com.itextpdf.text.pdf.parser.PdfImageObject;
import io.gitlab.donespeak.tutorial.pdf.reducesize.imagecompress.ImageCompressor;
import io.gitlab.donespeak.tutorial.pdf.reducesize.imagecompress.SimpleCompress;
import io.gitlab.donespeak.tutorial.pdf.util.ImageUtil;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 只能压缩jpeg
 *
 * @author Yang Guanrong
 */
public class ReducePdfSizeByCompressJpeg {

    private ImageCompressor compressor;
    private double quality;
    private double scale;
    /**
     * 将PNG转化为JPG，但是效果很差，透明背景会变成黑色
     */
    private boolean pngToJpg = false;

    public ReducePdfSizeByCompressJpeg(double quality, double scale) {
        this.compressor = new SimpleCompress();
        this.quality = quality;
        this.scale = scale;
    }

    public ReducePdfSizeByCompressJpeg(double quality, double scale, ImageCompressor compressor) {
        this.compressor = compressor;
        this.quality = quality;
        this.scale = scale;
    }

    public void setPngToJpg(boolean pngToJpg) {
        this.pngToJpg = pngToJpg;
    }

    public void replace(File pdf, File output) throws IOException, DocumentException {
        PdfReader reader = new PdfReader(new FileInputStream(pdf));
        int n = reader.getXrefSize();
        PdfObject object;
        PRStream stream;

        for (int i = 0; i < n; i++) {
            object = reader.getPdfObject(i);
            stream = findImageStream(object, pngToJpg);
            if (stream == null) {
                continue;
            }
            System.out.println("PdfReader#Xref: " + i);
            replaceImage(stream);
        }

        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(output));
        // furtherCompress(reader, stamper);
        stamper.close();
    }

    /**
     * 该方法遍历所有的对象，从而抽取stream图片类型的对象。
     * 因为这个原因，且png是通过两个stream进行存储的，所以会抽取出两张图片。
     */
    public void extractImages(File pdf, File imageDir) throws IOException, DocumentException {
        PdfReader reader = new PdfReader(new FileInputStream(pdf));
        int n = reader.getXrefSize();
        PdfObject object;
        PRStream stream;

        for (int i = 0; i < n; i++) {

            object = reader.getPdfObject(i);
            stream = findImageStream(object, true);
            if (stream == null) {
                continue;
            }
            PdfImageObject pdfImageObject = new PdfImageObject(stream);
            BufferedImage bi = pdfImageObject.getBufferedImage();
            if (bi == null) {
                continue;
            }
            System.out.println("PdfReader#Xref: " + i + "," + pdfImageObject.getFileType());
            File file = new File(imageDir, "xref-" + i + "." + pdfImageObject.getFileType());
            ImageUtil.toFile(pdfImageObject, file);
        }
    }

    private void furtherCompress(PdfReader reader, PdfStamper stamper) throws DocumentException {
        reader.removeFields();
        reader.removeUnusedObjects();
        stamper.setFullCompression();
        stamper.getWriter().setCompressionLevel(PdfStream.DEFAULT_COMPRESSION);
    }

    private PRStream findImageStream(PdfObject object, boolean pngToJpg) {
        PRStream stream;
        if (object == null || !object.isStream()) {
            // 判断是否为流
            return null;
        }
        stream = (PRStream)object;
        if(!isImage(stream)) {
            return null;
        }
        if(isJPG(stream)) {
            return stream;
        }
        if(pngToJpg && isPNG(stream)) {
            return stream;
        }
        // if (PdfName.DCTDECODE.equals(filter)) {
        //     return PdfImageObject.ImageBytesType.JPG.getFileExtension();
        // } else if (PdfName.JPXDECODE.equals(filter)) {
        //     return PdfImageObject.ImageBytesType.JP2.getFileExtension();
        // } else if (PdfName.FLATEDECODE.equals(filter)) {
        //     return PdfImageObject.ImageBytesType.PNG.getFileExtension();
        // } else if (PdfName.LZWDECODE.equals(filter)) {
        //     return PdfImageObject.ImageBytesType.CCITT.getFileExtension();
        // }
        return null;
    }

    private boolean isImage(PRStream stream) {
        System.out.println(stream.getAsName(PdfName.SUBTYPE));
        return PdfName.IMAGE.equals(stream.getAsName(PdfName.SUBTYPE));
    }

    private boolean isJPG(PRStream stream) {
        PdfName pdfName = stream.getAsName(PdfName.FILTER);
        return PdfName.DCTDECODE.equals(pdfName);
    }

    private boolean isPNG(PRStream stream) {
        PdfName pdfName = stream.getAsName(PdfName.FILTER);
        return PdfName.FLATEDECODE.equals(pdfName);
    }

    private void replaceImage(PRStream stream) throws IOException {

        PdfImageObject pdfImageObject = new PdfImageObject(stream);
        BufferedImage bi = pdfImageObject.getBufferedImage();
        if (bi == null) {
            return;
        }
        System.out.println(pdfImageObject.getFileType());

        BufferedImage resultImage = compressor.compress(bi, pdfImageObject.getFileType(), quality, scale);

        if(isJPG(stream)) {
            replaceJpgImage(stream, resultImage);
        } else if(pngToJpg && isPNG(stream)){
            // the background of the image will be black
            replaceJpgImage(stream, resultImage);
        } else if(isPNG(stream)){
            replacePngImage(stream, resultImage);
        }
    }

    private void replaceJpgImage(PRStream stream, BufferedImage resultImage) throws IOException {

        ByteArrayOutputStream imgBytes = new ByteArrayOutputStream();
        ImageIO.write(resultImage, "JPG", imgBytes);

        stream.clear();
        stream.setData(imgBytes.toByteArray(), false, PRStream.NO_COMPRESSION);
        stream.put(PdfName.TYPE, PdfName.XOBJECT);
        stream.put(PdfName.SUBTYPE, PdfName.IMAGE);
        stream.put(PdfName.FILTER, PdfName.DCTDECODE);
        stream.put(PdfName.WIDTH, new PdfNumber(resultImage.getWidth()));
        stream.put(PdfName.HEIGHT, new PdfNumber(resultImage.getHeight()));
        stream.put(PdfName.BITSPERCOMPONENT, new PdfNumber(8));
        stream.put(PdfName.COLORSPACE, PdfName.DEVICERGB);
    }

    private void replacePngImage(PRStream stream, BufferedImage resultImage) throws IOException {
        // do nothing
    }
}
