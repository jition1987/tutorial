package io.gitlab.donespeak.tutorial.pdf.reducesize;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PRIndirectReference;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

/**
 * 全部都没有用
 * @author Yang Guanrong
 */
public class ReplaceMultipleDifferentImages {

    public static void main(String[] args) throws DocumentException, IOException {
        File pdf = new File("pdf/asset/horse_2.pdf");
        File output = new File("pdf/target/output/" + ReplaceMultipleDifferentImages.class.getSimpleName() + "/horse_2.pdf");
        if(!output.getParentFile().exists()) {
            output.getParentFile().mkdirs();
        }
        replace(pdf, output);
    }

    private static void replace(File input, File output) throws DocumentException, IOException {
        PdfReader pdf = new PdfReader(input.getAbsolutePath());
        PdfStamper stp = new PdfStamper(pdf, new FileOutputStream(output));
        PdfWriter writer = stp.getWriter();
        Image img = Image.getInstance("pdf/asset/images/water.png");
        PdfDictionary pg = pdf.getPageN(1);
        PdfDictionary res = pg.getAsDict(PdfName.RESOURCES);
        PdfDictionary xobj = res.getAsDict(PdfName.XOBJECT);
        if (xobj != null) {
            for (Iterator<PdfName> it = xobj.getKeys().iterator(); it.hasNext(); ) {
                PdfObject obj = xobj.get(it.next());
                if (obj.isIndirect()) {
                    PdfDictionary tg = (PdfDictionary)PdfReader.getPdfObject(obj);
                    PdfName type = tg.getAsName(PdfName.SUBTYPE);
                    if (PdfName.IMAGE.equals(type)) {
                        PdfReader.killIndirect(obj);
                        Image maskImage = img.getImageMask();
                        if (maskImage != null) {
                            writer.addDirectImageSimple(maskImage);
                        }
                        writer.addDirectImageSimple(img, (PRIndirectReference)obj);
                        break;
                    }
                }
            }
        }
    }

    public static void replace2(File input, File output) throws DocumentException, IOException {
        PdfReader pdf = new PdfReader(input.getAbsolutePath());
        PdfStamper stp = new PdfStamper(pdf, new FileOutputStream(output));
        PdfWriter writer = stp.getWriter();
        Image img = Image.getInstance("pdf/asset/images/water.png");
        PdfDictionary pg = pdf.getPageN(1);
        PdfDictionary res =
            (PdfDictionary)PdfReader.getPdfObject(pg.get(PdfName.RESOURCES));
        PdfDictionary xobj =
            (PdfDictionary)PdfReader.getPdfObject(res.get(PdfName.XOBJECT));
        if (xobj != null) {
            for (Iterator it = xobj.getKeys().iterator(); it.hasNext(); ) {
                PdfObject obj = xobj.get((PdfName)it.next());
                if (obj.isIndirect()) {
                    PdfDictionary tg = (PdfDictionary)PdfReader.getPdfObject(obj);
                    PdfName type =
                        (PdfName)PdfReader.getPdfObject(tg.get(PdfName.SUBTYPE));
                    if (PdfName.IMAGE.equals(type)) {
                        PdfReader.killIndirect(obj);
                        Image maskImage = img.getImageMask();
                        if (maskImage != null) {
                            writer.addDirectImageSimple(maskImage);
                        }
                        writer.addDirectImageSimple(img, (PRIndirectReference)obj);
                        break;
                    }
                }
            }
        }
    }

    public static void replace3(File input, File output) throws DocumentException, IOException {
        PdfReader pdf = new PdfReader(input.getAbsolutePath());
        PdfStamper stp = new PdfStamper(pdf, new FileOutputStream(output));
        PdfWriter writer = stp.getWriter();
        Image img = Image.getInstance("pdf/asset/images/water.png");
        PdfDictionary pg = pdf.getPageN(1);
        PdfDictionary res = pg.getAsDict(PdfName.RESOURCES);
        PdfDictionary xobj = res.getAsDict(PdfName.XOBJECT);
        if (xobj != null) {
            for (Iterator<PdfName> it = xobj.getKeys().iterator(); it.hasNext(); ) {
                PdfObject obj = xobj.get(it.next());
                if (obj.isIndirect()) {
                    PdfDictionary tg = (PdfDictionary)PdfReader.getPdfObject(obj);
                    PdfName type = tg.getAsName(PdfName.SUBTYPE);
                    if (PdfName.IMAGE.equals(type)) {
                        // PdfReader.killIndirect(obj);
                        Image maskImage = img.getImageMask();
                        if (maskImage != null) {
                            writer.addDirectImageSimple(maskImage);
                        }
                        writer.addDirectImageSimple(img, (PRIndirectReference)obj);
                        break;
                    }
                }
            }
        }
    }
}
