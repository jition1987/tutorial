package io.gitlab.donespeak.tutorial.pdf.core;

import com.itextpdf.text.pdf.parser.ImageRenderInfo;
import com.itextpdf.text.pdf.parser.RenderListener;
import com.itextpdf.text.pdf.parser.TextRenderInfo;

/**
 * @author Yang Guanrong
 */
public interface DefaultRenderListener extends RenderListener {
    @Override
    default void beginTextBlock() {}

    @Override
    default void renderText(TextRenderInfo renderInfo) {}

    @Override
    default void endTextBlock() {}

    @Override
    default void renderImage(ImageRenderInfo renderInfo) {}
}
