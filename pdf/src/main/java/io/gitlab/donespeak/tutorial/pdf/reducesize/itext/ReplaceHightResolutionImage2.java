package io.gitlab.donespeak.tutorial.pdf.reducesize.itext;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PRStream;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfStream;
import com.itextpdf.text.pdf.parser.PdfImageObject;
import io.gitlab.donespeak.tutorial.pdf.reducesize.imagecompress.ImageCompressor;
import io.gitlab.donespeak.tutorial.pdf.reducesize.imagecompress.SimpleCompress;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author Yang Guanrong
 */
public class ReplaceHightResolutionImage2 {

    private ImageCompressor compressor;
    private double quality;
    private double scale;

    public ReplaceHightResolutionImage2(double quality, double scale) {
        this.compressor = new SimpleCompress();
        this.quality = quality;
        this.scale = scale;
    }

    public ReplaceHightResolutionImage2(double quality, double scale, ImageCompressor compressor) {
        this.compressor = compressor;
        this.quality = quality;
        this.scale = scale;
    }

    public void replace(File pdf, File output) throws IOException, DocumentException {
        PdfReader reader = new PdfReader(new FileInputStream(pdf));
        int n = reader.getXrefSize();
        PdfObject object;
        PRStream stream;

        for (int i = 0; i < n; i++) {

            object = reader.getPdfObject(i);
            stream = findImageStream(object);
            if (stream == null) {
                continue;
            }

            PdfImageObject pdfImageObject = new PdfImageObject(stream);
            System.out.println("PdfReader#Xref: " + i + "," + pdfImageObject.getFileType());
            replaceImage(stream, pdfImageObject);
        }

        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(output));
        // furtherCompress(reader, stamper);
        stamper.close();
    }

    private void furtherCompress(PdfReader reader, PdfStamper stamper) throws DocumentException {
        reader.removeFields();
        reader.removeUnusedObjects();
        stamper.setFullCompression();
        stamper.getWriter().setCompressionLevel(PdfStream.DEFAULT_COMPRESSION);
    }

    private PRStream findImageStream(PdfObject object) {
        PRStream stream;
        if (object == null || !object.isStream()) {
            // 判断是否为流
            return null;
        }
        stream = (PRStream)object;
        System.out.println(stream.getAsName(PdfName.SUBTYPE));
        if (!PdfName.IMAGE.equals(stream.getAsName(PdfName.SUBTYPE))) {
            return null;
        }
        PdfName pdfName = stream.getAsName(PdfName.FILTER);
        if (!PdfName.DCTDECODE.equals(pdfName) && !PdfName.FLATEDECODE.equals(pdfName)) {
            return null;
        }
        // if (PdfName.DCTDECODE.equals(filter)) {
        //     return PdfImageObject.ImageBytesType.JPG.getFileExtension();
        // } else if (PdfName.JPXDECODE.equals(filter)) {
        //     return PdfImageObject.ImageBytesType.JP2.getFileExtension();
        // } else if (PdfName.FLATEDECODE.equals(filter)) {
        //     return PdfImageObject.ImageBytesType.PNG.getFileExtension();
        // } else if (PdfName.LZWDECODE.equals(filter)) {
        //     return PdfImageObject.ImageBytesType.CCITT.getFileExtension();
        // }
        return stream;
    }

    private void replaceImage(PRStream stream, PdfImageObject pdfImageObject) throws IOException {

        BufferedImage img = pdfImageObject.getBufferedImage();
        if (img == null) {
            return;
        }

        BufferedImage bufferedImage = compressor.compress(img, pdfImageObject.getFileType(), quality, scale);

        System.out.println("img(w, h): (" + img.getWidth() + "," + img.getHeight() + ")");
        System.out.println("bufferedImage(w, h): (" + bufferedImage.getWidth() + "," + bufferedImage.getHeight() + ")");

        // TODO how to replace the stream
    }
}
