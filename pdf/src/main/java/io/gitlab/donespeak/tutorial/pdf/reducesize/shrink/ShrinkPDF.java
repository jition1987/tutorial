package io.gitlab.donespeak.tutorial.pdf.reducesize.shrink;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.io.RandomAccessBufferedFileInputStream;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.form.PDFormXObject;
import org.apache.pdfbox.pdmodel.graphics.image.JPEGFactory;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

/**
 * @author Yang Guanrong
 */
public class ShrinkPDF {

    // -- Fields --

    private File input;
    private float compQual = -1;
    private static float compQualDefault = 0.85f;
    private boolean headless = false;
    private boolean tiff = false;

    public ShrinkPDF(File input, float compQual) throws ShrinkerException {
        this(input, compQual, false, false);
    }

    public ShrinkPDF(File input) throws ShrinkerException {
        this(input, compQualDefault, false, false);
    }

    public ShrinkPDF(File input, float compQual, boolean headless, boolean tiff) throws ShrinkerException {
        setInput(input);
        setCompQual(compQual);
        setHeadless(headless);
        setTiff(tiff);
    }

    /**
     * Set the input file.
     * @throws ShrinkerException if the file does not exist or
     *             cannot be read.
     */
    public void setInput (final File f) throws ShrinkerException {
        if(f == null || !f.canRead()) {
            throw new ShrinkerException("Can't read input file: " + f == null ? "<null>" : f.toString());
        }
        this.input = f;
    }

    /**
     * Set the compression quality parameter.
     * @param compQual Number between 0 (low quality, small file size)
     *                 and 1 (high quality, large file size)
     * @throws ShrinkerException if {@code compQual} is out of bounds
     */
    public void setCompQual(final float compQual) throws ShrinkerException {
        if(0 > compQual || compQual > 1) {
            throw new ShrinkerException("Compression quality must be between 0 and 1");
        }
        this.compQual = compQual;
    }

    /**
     * Set {@code true} to prohibit the use of dialogs.
     */
    public void setHeadless(final boolean headless) {
        this.headless = headless;
    }

    /**
     * Set {@code true} to embed images as uncompressed TIFFs.
     */
    public void setTiff(final boolean tiff) {
        this.tiff = tiff;
    }

    /**
     * Shrink a PDF
     * @param f {@code File} pointing to the PDF to shrink
     * @param compQual Compression quality parameter. 0 is
     *                 smallest file, 1 is highest quality.
     * @return The compressed {@code PDDocument}
     * @throws FileNotFoundException
     * @throws IOException
     */
    public PDDocument shrinkMe()
        throws FileNotFoundException, IOException {
        if(compQual < 0) {
            compQual = compQualDefault;
        }
        final RandomAccessBufferedFileInputStream rabfis =
            new RandomAccessBufferedFileInputStream(input);
        final PDFParser parser = new PDFParser(rabfis);
        parser.parse();
        final PDDocument doc = parser.getPDDocument();
        final PDPageTree pages = doc.getPages();
        final ImageWriter imgWriter;
        final ImageWriteParam iwp;
        if(tiff) {
            final Iterator<ImageWriter> tiffWriters =
                ImageIO.getImageWritersBySuffix("png");
            imgWriter = tiffWriters.next();
            iwp = imgWriter.getDefaultWriteParam();
            // iwp.setCompressionMode(ImageWriteParam.MODE_DISABLED);
            // iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            // iwp.setCompressionQuality(compQual);
        } else {
            final Iterator<ImageWriter> jpgWriters =
                ImageIO.getImageWritersByFormatName("jpeg");
            imgWriter = jpgWriters.next();
            iwp = imgWriter.getDefaultWriteParam();
            iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            iwp.setCompressionQuality(compQual);
        }
        for(PDPage p : pages) {
            scanResources(p.getResources(), doc, imgWriter, iwp);
        }
        return doc;
    }

    private void scanResources(
        final PDResources rList,
        final PDDocument doc,
        final ImageWriter imgWriter,
        final ImageWriteParam iwp)
        throws FileNotFoundException, IOException {
        Iterable<COSName> xNames = rList.getXObjectNames();
        for(COSName xName : xNames) {
            final PDXObject xObj = rList.getXObject(xName);
            if(xObj instanceof PDFormXObject) {
                scanResources(((PDFormXObject)xObj).getResources(), doc, imgWriter, iwp);
            }
            if(!(xObj instanceof PDImageXObject)) {
                continue;
            }
            final PDImageXObject img = (PDImageXObject)xObj;
            System.out.println("Compressing image: " + xName.getName());
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            imgWriter.setOutput(ImageIO.createImageOutputStream(baos));
            imgWriter.write(null,
                new IIOImage(img.getImage(), null, null), iwp);
            final ByteArrayInputStream bais =
                new ByteArrayInputStream(baos.toByteArray());
            final PDImageXObject imgNew;
            if(tiff) {
                imgNew = LosslessFactory.createFromImage(doc, img.getImage());
            } else {
                imgNew = JPEGFactory.createFromStream(doc, bais);
            }
            rList.put(xName, imgNew);
        }
    }
}
