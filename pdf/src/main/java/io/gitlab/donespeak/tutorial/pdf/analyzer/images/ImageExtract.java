package io.gitlab.donespeak.tutorial.pdf.analyzer.images;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Yang Guanrong
 * @date 2020/05/03 16:08
 */
public interface ImageExtract {

    void extract(File pdf, File imageDir) throws IOException;

    void extract(InputStream pdf, File imageDir) throws IOException;
}
