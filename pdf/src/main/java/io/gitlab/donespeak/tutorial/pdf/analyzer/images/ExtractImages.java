package io.gitlab.donespeak.tutorial.pdf.analyzer.images;

import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.ImageRenderInfo;
import com.itextpdf.text.pdf.parser.PdfImageObject;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import io.gitlab.donespeak.tutorial.pdf.core.DefaultRenderListener;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * But when you try this example on your own system, you’ll notice that not all images are extracted.
 * Please don’t report this as a bug. Not all the different types of images are supported yet. This is only a preview of new functionality that has been added to iText
 * recently. Just like with parsing text, a best effort is done; when more types of images
 * are supported will depend on code contributors and paying iText users.
 *
 * @author Yang Guanrong
 */
public class ExtractImages implements ImageExtract {

    @Override
    public void extract(File pdf, File imgDir) throws IOException {
        if(!imgDir.exists()) {
            imgDir.mkdirs();
        }
        PdfReader reader = new PdfReader(pdf.getAbsolutePath());
        PdfReaderContentParser parser = new PdfReaderContentParser(reader);
        MyImageRenderListener listener = new MyImageRenderListener(imgDir);
        for (int i = 1; i <= reader.getNumberOfPages(); i++) {
            parser.processContent(i, listener);
        }
    }

    @Override public void extract(InputStream pdf, File imageDir) throws IOException {

    }

    public static class MyImageRenderListener implements DefaultRenderListener {
        protected String path;

        public MyImageRenderListener(File imgDir) {
            this.path = imgDir.getAbsolutePath() + "/img-%s.%s";
        }

        @Override
        public void renderImage(ImageRenderInfo renderInfo) {
            try {
                String filename;
                PdfImageObject image = renderInfo.getImage();
                PdfName filter = (PdfName) image.get(PdfName.FILTER);
                if (PdfName.DCTDECODE.equals(filter)) {
                    // JPEG IMAGE
                    filename = String.format(path, renderInfo.getRef().getNumber(), "jpg");
                    try (FileOutputStream os = new FileOutputStream(filename)) {
                        os.write(image.getImageAsBytes());
                        os.flush();
                    }
                } else if (PdfName.JPXDECODE.equals(filter)) {
                    // JPEG2000 images
                    filename = String.format(path, renderInfo.getRef().getNumber(), "jp2");
                    try (FileOutputStream os = new FileOutputStream(filename)) {
                        os.write(image.getImageAsBytes());
                        os.flush();
                    }
                } else {
                    PdfImageObject img = renderInfo.getImage();
                    BufferedImage bufferedImage = renderInfo.getImage().getBufferedImage();
                    System.out.println(img.getFileType() + "-" + bufferedImage.getColorModel().getNumComponents() + "-" + bufferedImage.getColorModel().getColorSpace());
                    filename = String.format(path, renderInfo.getRef().getNumber(), renderInfo.getImage().getFileType());
                    ImageIO.write(bufferedImage, renderInfo.getImage().getFileType(), new File(filename));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
