package io.gitlab.donespeak.tutorial.pdf.reducesize;

import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class RemoveAllImageFromPdfTest {

    private static File pdf = new File("/Users/yangguanrong/Downloads/testpdf/report.pdf");
    // private static File pdf = new File("asset/image_in_header.pdf");
    private static File pdfCompressed = new File("target/output/" + RemoveAllImageFromPdf.class.getSimpleName(), fileName(pdf) + "-compressed.pdf");
    private static File resultDir = new File("target/output/" + RemoveAllImageFromPdf.class.getSimpleName() + "/");

    @Test
    public void testCompress() throws IOException {
        System.out.println(pdf.getAbsolutePath());
        if(resultDir.exists()) {
            resultDir.delete();
        }
        RemoveAllImageFromPdf.extractImages(pdf, new File(resultDir, fileName(pdf)));
        RemoveAllImageFromPdf.compress(pdf, pdfCompressed);
        // RemoveAllImageFromPdf.extractImages(pdfCompressed, new File(resultDir, fileName(pdfCompressed)));
    }

    private static String fileName(File file) {
        return file.getName().substring(0, file.getName().lastIndexOf("."));
    }

}