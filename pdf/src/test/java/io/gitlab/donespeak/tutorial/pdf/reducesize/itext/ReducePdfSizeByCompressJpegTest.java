package io.gitlab.donespeak.tutorial.pdf.reducesize.itext;

import com.itextpdf.text.DocumentException;
import io.gitlab.donespeak.tutorial.pdf.reducesize.imagecompress.ThumbnailatorCompressor;
import io.gitlab.donespeak.tutorial.pdf.reducesize.util.CommonUtil;
import io.gitlab.donespeak.tutorial.pdf.util.FileAssistant;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class ReducePdfSizeByCompressJpegTest {

    @Test
    public void reduceWithThumbnailatorCompressor() throws IOException, DocumentException {
        double quality = 1d;
        double scale = 0.5d;
        // File pdf = new File("/Users/yangguanrong/Codes/Gitlab/tutorial/pdf/asset/horse_releaxed_reuseimage_20.pdf");
        File pdf = new File("/Users/yangguanrong/Codes/Gitlab/tutorial/pdf/asset/horse_relaxed.pdf");
        File output = new File("/Users/yangguanrong/Codes/Gitlab/tutorial/pdf/target/output/" +  this.getClass().getSimpleName(),
            FileAssistant.getFileName(pdf) + "-" + quality + "-" + scale + ".pdf");

        FileAssistant.createParentDirIfNonExist(output);

        ReducePdfSizeByCompressJpeg replacer = new ReducePdfSizeByCompressJpeg(quality, scale, new ThumbnailatorCompressor());
        replacer.setPngToJpg(false);
        replacer.replace(pdf, output);

        extractImage(replacer, output);
    }

    private void extractImage(ReducePdfSizeByCompressJpeg replacer, File pdf) throws IOException, DocumentException {

        File imageDir = new File(pdf.getParent(), FileAssistant.getFileName(pdf));

        FileAssistant.removeIfExist(imageDir);
        FileAssistant.createDirIfNonExist(imageDir);

        replacer.extractImages(pdf, imageDir);
    }

    public void replace() throws IOException, DocumentException {
        double quality = 0.8;
        double scale = 0.5;
        File input = new File("/Users/yangguanrong/Downloads/testpdf/report.pdf");
        File output = CommonUtil.getCompressName(input, "replaced-" + quality);
        ReducePdfSizeByCompressJpeg replacer = new ReducePdfSizeByCompressJpeg(quality, scale);
        replacer.replace(input, output);
    }
}