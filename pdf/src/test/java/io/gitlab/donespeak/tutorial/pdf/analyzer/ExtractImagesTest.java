package io.gitlab.donespeak.tutorial.pdf.analyzer;

import io.gitlab.donespeak.tutorial.pdf.analyzer.images.ExtractImages;
import io.gitlab.donespeak.tutorial.pdf.analyzer.images.ImageExtract;

import java.io.File;
import java.io.IOException;

public class ExtractImagesTest {

    public static void main(String[] args) throws IOException {
        File pdf = new File("pdf/asset/horse.pdf");
        File imageDir = new File("pdf/target/output/horse/images/" + ExtractImagesTest.class.getSimpleName());
        if(imageDir.exists()) {
            imageDir.delete();
        }
        imageDir.mkdirs();
        ImageExtract extractor = new ExtractImages();
        extractor.extract(pdf, imageDir);
    }
}