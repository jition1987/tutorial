package io.gitlab.donespeak.tutorial.pdf.reducesize.util;

import java.io.File;

/**
 * @author Yang Guanrong
 */
public class CommonUtil {

    public static File getCompressName(File file, String ... appends) {
        String fullName = file.getName();
        int lastDot = fullName.lastIndexOf(".");
        String apendSufix = String.join("-", appends);
        if(lastDot < 0) {
            return new File(file.getParentFile(), file.getName() + "-" + apendSufix);
        }
        String name = fullName.substring(0, lastDot);
        String extension = fullName.substring(lastDot);
        return new File(file.getParentFile(), name + "-" + apendSufix + extension);
    }
}
