package io.gitlab.donespeak.tutorial.pdf.reducesize.shrink;

import org.apache.pdfbox.pdmodel.PDDocument;

import java.io.File;
import java.io.IOException;

public class ShrinkPDFTest {

    public static void main(String[] args) throws ShrinkerException, IOException {
        File input = new File("pdf/asset/horse.pdf");
        File output = new File("pdf/target/output/reduce/shrinked/horse.pdf");
        if(!output.getParentFile().exists()) {
            output.getParentFile().mkdirs();
        }
        float camQue = 0.3f;
        ShrinkPDF shrinkPDF2 = new ShrinkPDF(input, camQue, false, true); // new ShrinkPDF2(input, camQue, true, false);
        PDDocument pdf = shrinkPDF2.shrinkMe();
        pdf.save(output);
    }
}