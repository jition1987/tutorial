package io.gitlab.donespeak.tutorial.pdf.analyzer.images;

import java.io.File;
import java.io.IOException;

public class ExtractImagesReplaceHightResolutionImageTest {

    public static void main(String[] args) throws IOException {
        File pdf = new File("/Users/yangguanrong/Desktop/report-compressed.pdf");
        File imageDir = new File(pdf.getParentFile(), "report-compressed/images");
        if(imageDir.exists()) {
            imageDir.delete();
        }
        ImageExtract extractor = new ExtractImagesReplaceHightResolutionImage();
        extractor.extract(pdf, imageDir);
    }
}