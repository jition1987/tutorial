package io.gitlab.donespeak.tutorial.pdf.reducesize.itext;

import com.itextpdf.text.DocumentException;
import io.gitlab.donespeak.tutorial.pdf.util.FileAssistant;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class ReplaceHightResolutionImage2Test {

    // private static File pdf = new File("pdf/asset/horse_relaxed.pdf");
    private static File pdf = new File("asset/horse_relaxed.pdf");
    private static File pdfCompressed = new File("target/output/" + ReplaceHightResolutionImage2Test.class.getSimpleName(), FileAssistant.getFileName(pdf) + "-compressed.pdf");
    private static File resultDir = new File("target/output/" + ReplaceHightResolutionImage2Test.class.getSimpleName() + "/");

    @Test
    public void replace() throws IOException, DocumentException {
        FileAssistant.createParentDirIfNonExist(pdfCompressed);

        ReplaceHightResolutionImage2 resolutionImage2 = new ReplaceHightResolutionImage2(0.5, 0.5);
        resolutionImage2.replace(pdf, pdfCompressed);
    }
}