package io.gitlab.donespeak.tutorial.pdf.reducesize.dpi;

import io.gitlab.donespeak.tutorial.pdf.reducesize.RemoveAllImageFromPdf;
import io.gitlab.donespeak.tutorial.pdf.util.FileAssistant;
import net.coobird.thumbnailator.Thumbnails;

import java.io.File;
import java.io.IOException;

public class DpiReduceWithEngineTest {

    // private static File pdf = new File("/Users/yangguanrong/Downloads/testpdf/report.pdf");
    private static File pdf = new File("asset/svg.pdf");
    private static File pdfCompressed = new File("target/output/" + DpiReduceWithEngineTest.class.getSimpleName(), FileAssistant.getFileName(pdf) + "-compressed-72-0.5dpi.pdf");
    private static File resultDir = new File("target/output/" + DpiReduceWithEngineTest.class.getSimpleName() + "/");

    // @Test
    public void reduce() throws IOException {
        // FileAssistant.createParentDirIfNonExist(pdfCompressed);

        // DpiReduceWithEngine.reduce(pdf, 72, pdfCompressed);

        RemoveAllImageFromPdf.extractImages(
            new File("/Users/yangguanrong/Work/Bees360/Data/report-for-compressed/1581108839845Full-scopeUnderwritingReport.pdf__25722_63176193.pdf"),
            new File("/Users/yangguanrong/Work/Bees360/Data/report-for-compressed/1581108839845Full-scopeUnderwritingReport.pdf__25722_63176193/"));
    }

    public static void main(String[] args) throws IOException {
        File image = new File("/Users/yangguanrong/Work/Bees360/Data/report-for-compressed/1581108839845Full-scopeUnderwritingReport.pdf__25722_63176193/1-I0-DeviceRGB-147382523223592.png");
        File result = new File("/Users/yangguanrong/Work/Bees360/Data/report-for-compressed/compressed/1-I0-DeviceRGB-147382523223592.png");
        // @formatter:off
        Thumbnails.of(image)
            // .imageType(image.getType())
            .scale(0.5)
            // .outputFormat(imageFormat)
            .useOriginalFormat()
            .toFile(result);
    }
}