package io.gitlab.donespeak.tutorial.pdf.analyzer.images;

import io.gitlab.donespeak.tutorial.pdf.util.FileAssistant;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class ExtractImageWithPDFStreamEngineTest {

    @Test
    public void extract() throws IOException {
        File pdf = new File("asset/horse_relaxed_reuseimage_20.pdf");
        File imageDir = new File("target/output/" + ExtractImageWithPDFStreamEngineTest.class.getSimpleName() + "/images");
        FileAssistant.removeIfExist(imageDir);
        FileAssistant.createDirIfNonExist(imageDir);
        ExtractImageWithPDFStreamEngine.extract(pdf, imageDir);
    }
}