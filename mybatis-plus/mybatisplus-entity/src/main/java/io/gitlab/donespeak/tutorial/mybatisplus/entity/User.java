package io.gitlab.donespeak.tutorial.mybatisplus.entity;

import lombok.Data;

/**
 * @author Yang Guanrong
 * @date 2019/11/18 19:07
 */
@Data
public class User {
    private Long id;
    private String name;
    private Integer age;
    private String email;
}