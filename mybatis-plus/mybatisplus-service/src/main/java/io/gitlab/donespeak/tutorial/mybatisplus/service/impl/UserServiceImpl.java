package io.gitlab.donespeak.tutorial.mybatisplus.service.impl;

import io.gitlab.donespeak.tutorial.mybatisplus.entity.User;
import io.gitlab.donespeak.tutorial.mybatisplus.mapper.UserMapper;
import io.gitlab.donespeak.tutorial.mybatisplus.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Yang Guanrong
 * @date 2019/11/18 18:32
 */
@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> list() {
        return userMapper.selectList(null);
    }

    @Override
    public User getById(long userId) {
        return userMapper.selectById(userId);
    }
}
