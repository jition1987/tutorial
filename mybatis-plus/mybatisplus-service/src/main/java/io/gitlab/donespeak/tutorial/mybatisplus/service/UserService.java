package io.gitlab.donespeak.tutorial.mybatisplus.service;

import io.gitlab.donespeak.tutorial.mybatisplus.entity.User;

import java.util.List;

/**
 * @author Yang Guanrong
 * @date 2019/11/18 18:31
 */
public interface UserService {
    List<User> list();
    User getById(long userId);
}