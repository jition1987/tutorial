package io.gitlab.donespeak.tutorial.mybatisplus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.gitlab.donespeak.tutorial.mybatisplus.entity.User;

/**
 * @author Yang Guanrong
 * @date 2019/11/18 19:07
 */
public interface UserMapper extends BaseMapper<User> {
}
