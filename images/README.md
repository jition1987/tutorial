# 图片

###### `donespeak` `2020/05/03`

## 图片类型

- png
- jpg
- jpeg
- bmp
- gif
- tng

## 在线工具

图片工具集：
- [Tiomg](https://tiomg.org/image)

图片压缩：
- [picdiet](https://www.picdiet.com/)

## 开源工具

### coobird/thumbnailator

[coobird/thumbnailator @github.com](https://github.com/coobird/thumbnailator)  

对不同该类型图片的处理效果  


工具：
- [google/guetzli](https://github.com/google/guetzli) C++实现

## 多媒体理论 - 图片

- [PNG图片压缩原理解析](https://juejin.im/post/5c8e4feb6fb9a070aa5ce200)
- [JPEG图像压缩算法流程详解](https://blog.csdn.net/qingkongyeyue/article/details/70443872)

## 文件类型判断

可以使用一个文本编辑工具如UltraEdit打开文件（16进制模式下），然后看文件头是什么字符，以下是常见文件类型的文件头字符(16进制)，
```java
JPEG (jpg)，文件头：FFD8FF 
PNG (png)，文件头：89504E47 
GIF (gif)，文件头：47494638 
TIFF (tif)，文件头：49492A00 
Windows Bitmap (bmp)，文件头：424D 
CAD (dwg)，文件头：41433130 
Adobe Photoshop (psd)，文件头：38425053 
Rich Text Format (rtf)，文件头：7B5C727466 
XML (xml)，文件头：3C3F786D6C 
HTML (html)，文件头：68746D6C3E 
Email [thorough only] (eml)，文件头：44656C69766572792D646174653A 
Outlook Express (dbx)，文件头：CFAD12FEC5FD746F 
Outlook (pst)，文件头：2142444E 
MS Word/Excel (xls.or.doc)，文件头：D0CF11E0 
MS Access (mdb)，文件头：5374616E64617264204A 
WordPerfect (wpd)，文件头：FF575043 
Postscript (eps.or.ps)，文件头：252150532D41646F6265 
Adobe Acrobat (pdf)，文件头：255044462D312E 
Quicken (qdf)，文件头：AC9EBD8F 
Windows Password (pwl)，文件头：E3828596 
ZIP Archive (zip)，文件头：504B0304 
RAR Archive (rar)，文件头：52617221 
Wave (wav)，文件头：57415645 
AVI (avi)，文件头：41564920 
Real Audio (ram)，文件头：2E7261FD 
Real Media (rm)，文件头：2E524D46 
MPEG (mpg)，文件头：000001BA 
MPEG (mpg)，文件头：000001B3 
Quicktime (mov)，文件头：6D6F6F76 
Windows Media (asf)，文件头：3026B2758E66CF11 
MIDI (mid)，文件头：4D546864
```

## 图片大小分辨率计算

```
一、图像占用空间的大小计算：
大小=分辨率*位深/8
分辨率=宽*高（如：1024*768,640*480）
位深：如24位，16位，8位
/8计算的是字节数。
例如：
一幅图像分辨率：1024*768,24位，则其大小计算如下：
大小=1024*768824/8=2359296byte=2304KB

二、图像物理尺寸的大小计算：
参考一些会员近期提出的问题，和冈萨雷斯的书，做出一些总结，希望对大家有所帮助，也希望大家多多补充。
1、厘米和像素
厘米和像素没任何关系，厘米是长度单位，什么是象素呢？像素是组成图像的最基本单元。它是一个小的方形的颜色块。
一个图像通常由许多像素组成，这些像素被排成横行或纵列，每个像素都是方形的。当你用缩放工具将图像放到足够大时，就可以看到类似马赛克的效果，每个小方块就是一个像素。
每个像素都有不同的颜色值。单位面积内的像素越多，分辨率(dpi)越高，图像的效果就越好。
显示器上正常显示的图像，当放大到一定比例后，就会看到类似马赛克的效果。每个小方块为一个像素，也可称为栅格。像素图的质量是由分辨率决定的，单位面积内的像素越多，分辨率越高，图像的效果就越好。
2、DPI计算
这幅图像分辨率200*200dpi，大小450*450像素，那么就可以得到：
图像大小 = 图像大小 / 分辨率 = 450 / 200 = 2.25
所以，这幅图像的大小为2.25*2.25英寸
如果要求图像大小变成1.5*1.5英寸，但像素数仍为450*450，按照公式：
图像大小 = 图像像素数 / 图像分辨率，就得到了图像的分辨率应为：450 / 1.5 = 300dpi
所以，应该使用命令imwrite(f, ‘sf.tif’, ‘compression’, ‘none’, ‘resolution’, [300 300])
3、计算方法验证
用Photoshop来查看：
可以看到，图像的像素数仍为450*450，但原图像的分辨率为200dpi，尺寸大小为2.25*2.25英寸，新图像的分辨率为300dpi，尺寸大小为1.5*1.5英寸；
新图像是450*450的像素分布在1.5*1.5英寸的区域内，这样的过程在打印文档时控制图像的大小而不牺牲其分辨率是很有用的。
```