package io.gitlab.donespeak.tutorial.concurrence;

/**
 * @author Yang Guanrong
 * @date 2020/04/01 23:06
 */
public class WriteOnRead {

	public static void main(String[] args) {
		String message = String.format("The company `%s(%s)` need to be registered in SymbilityConnect.", "dfdf", 121);
		System.out.println(message);
	}
}
