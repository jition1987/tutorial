package io.gitlab.donespeak.tutorial.concurrence;

import com.google.common.base.CaseFormat;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Yang Guanrong
 * @date 2019/12/13 18:00
 */
public class Main {

    public static class Test {

        public void test() {
            ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
            System.out.println("Before execute.");
            cachedThreadPool.execute(new Runnable() {

                @Override
                public void run() {
                    System.out.println("Test start ...");
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Test end");
                }
            });
            System.out.println("After execute.");
        }
    }

    public static void main(String[] args) throws InterruptedException {
        String[] dirs = {"user.home", "user.dir", "java.io.tmpdir"};
        for(String dir: dirs) {
            System.out.println(System.getProperty(dir));
        }
        System.out.println("Before test ...");
        Test test = new Test();
        test.test();
        System.out.println("After test.");
        Thread.sleep(5000);
        System.out.println("Main end.");
        System.out.println(CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, "book-name"));
        System.out.println(CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, "book-name"));
        System.out.println(CaseFormat.LOWER_HYPHEN.to(CaseFormat.LOWER_CAMEL, "book-name"));
        System.out.println(CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_HYPHEN, "book-name"));

    }
}
